package com.example.studentscheduler;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.studentscheduler.Model.InitialValuesActivity;

/**
 * Created by dkell40 on 11/12/2017.
 *     compileSdkVersion 25
 buildToolsVersion "25.0.2"
 minSdkVersion 16
 targetSdkVersion 25
 */

public class TermDetails extends InitialValuesActivity {


    private String termTitle;
    private String totalCu;
    private String start;
    private String end;
    private String termStatus;

    //private Cursor termCursor;
    private Cursor allTerms;


    private Button course_save;
    private Spinner spinnerTitle, spinnerStatus;
    private Button buttonStart, buttonEnd;
    private Boolean isEditable;

    private TextView cuCountTextView;

    /**
     * From InitialValuesActivity class launchActivity
     */

    @Override
    public void launchActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.putExtra("termId", termId);
        startActivity(intent);
    }

    /**
     * From AppCompatActivity class on start, initializes variables.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_details);
        launchSubMenuBar(this);

        Bundle resources = getIntent().getExtras();
        if (resources != null) {
            termId = resources.getString("termId");
            termTitle = resources.getString("termTitle");
            totalCu = resources.getString("totalCu");

            isEditable = new Boolean(resources.getString("isEditable"));
        }


        spinnerTitle = (Spinner)findViewById(R.id.term_name);
        spinnerStatus = (Spinner)findViewById(R.id.status);
        buttonStart = (Button)findViewById(R.id.course_start);
        buttonEnd = (Button)findViewById(R.id.course_end);


        cuCountTextView = (TextView) findViewById(R.id.cu_count);


        course_save = (Button) findViewById(R.id.course_save);

        course_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateTerm();
            }
        });

        allTerms = getCursor("*", "", "term");
        /*termCourses = getCursor("*", " WHERE termid ='"+termId+"'", "courses");
        courseCount = new Integer(termCourses.getCount()).toString();*/

        setItems();

        setEditEnabled(isEditable);
    }
    /**
     * From AppCompatActivity class onSaveInstanceState
     */
    @Override
    protected void onSaveInstanceState(Bundle portraitOrLandscape) {
        Log.d("TermDetails.class 84", "isEditable="+isEditable+"");
        portraitOrLandscape.putString("isEditable", isEditable.toString());
        super.onSaveInstanceState(portraitOrLandscape);

    }
    /**
     * From AppCompatActivity class onRestoreInstanceState
     */
    @Override
    public void onRestoreInstanceState(Bundle resources ){
        if(resources !=null) {
            isEditable = new Boolean(resources.getString("isEditable"));
        }
        setEditEnabled(isEditable);
        super.onResume();
    }

    /**
     * From View.InitialValuesActivity class setEditEnabled
     */
   @Override
    public void setEditEnabled(boolean value){

        isEditable = value;
        spinnerTitle.setEnabled(value);
        spinnerStatus.setEnabled(value);
        buttonStart.setEnabled(value);
        buttonEnd.setEnabled(value);
        getVisibility(value);

    }

    /**
     * getter for button visible
     */
    private void getVisibility(boolean value){
        if(value)
            course_save.setVisibility(View.VISIBLE);
        else
            course_save.setVisibility(View.INVISIBLE);

    }

    /**
     * Adds items to the list adapter container for rendering.
     */
    private void setItems(){
        String[] titlesList = getTermTitle(allTerms);

        setSpinner(R.layout.spinner_dropdown_header, spinnerTitle, termTitle, titlesList);
        setSpinner(R.layout.spinner_dropdown_normal, spinnerStatus, termStatus, status);

        buttonStart.setText(start);
        buttonEnd.setText(end);

        cuCountTextView.setText(totalCu);//+termDetails
    }


    /**
     * getter for term
     */
    public String[] getTermTitle(Cursor cursor){

        String[] data = new String[cursor.getCount()];
        int i = 0;
        if(cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Log.d("Initial.class 197:", "cursor.getString(1) is "+cursor.getString(1) );
                data[i] = cursor.getString(1);
                if(cursor.getString(0).equals(termId)) {
                    termTitle = cursor.getString(1);
                    start = cursor.getString(2);
                    end = cursor.getString(3);
                    termStatus = cursor.getString(4);
                }
                i++;
            }
        }else{
            Log.d("Initial.class 201:", "cursor.getCount() is 0" );
        }
        return data;
    }

    /**
     * Updates a term to sqlite database.
     */
    public void updateTerm() {

        try{

            nullCheck(spinnerTitle.getSelectedItem().toString());
            String title = validateDuplicateTerm(spinnerTitle.getSelectedItem().toString(), termTitle);
            String status = nullCheck(spinnerStatus.getSelectedItem().toString());

            nullCheck(buttonStart.getText().toString());
            nullCheck(buttonEnd.getText().toString());

            //Log.d("spinnerStatus 254", spinnerStatus.getPrompt().toString());
            Log.d("UPDATE COURSE table", "course");
            Log.d("UPDATE COURSE 0", termId.toString());
            Log.d("UPDATE COURSE 1", title);
            Log.d("UPDATE COURSE 2", buttonStart.getText().toString());
            Log.d("UPDATE COURSE 3", buttonEnd.getText().toString());
            Log.d("UPDATE COURSE 4", status);


            long isUpdate = sqlIte.updateData(

                    "term",
                    termId.toString(),
                    title,
                    buttonStart.getText().toString(),
                    buttonEnd.getText().toString(),
                    status
            );

            if(isUpdate > 0) {
                Log.d("UPDATE TERM updated", isUpdate+"");
                Toast.makeText(TermDetails.this, "Data Update", Toast.LENGTH_LONG).show();
                restart();
            }
            else {
                Log.d("UPDATE TERM failed", isUpdate + "");
                Toast.makeText(TermDetails.this, "Data not Updated", Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            showAlert("Warning",e.getMessage());

        }
    }

    /**
     * Resets the view and repopulated the list.
     */
    public void restart(){
        launchActivity(MainNavigationPanel.class);
        launchActivity(TermList.class);
        launchActivity(TermDetails.class);
        this.finish();
    }

    /**
     * From View.OnClickListener interface onClick
     */
    @Override
    public void onClick(View v) {
    //for alert dates only
    }

    /**
     * setter for start date
     */
    public void setStartDate(View view) {
        year = getDeviceYear();
        month = getDeviceMonth();
        day = getDeviceDay();
        Dialog dialog = new DatePickerDialog(this, R.style.datepicker, myStartListener, year, month, day);
        dialog.show();
        Toast.makeText(getApplicationContext(), "set start date",
                Toast.LENGTH_SHORT)
                .show();
    }

    /**
     * setter for start date
     */
    public void setEndDate(View view) {
        year = getDeviceYear();
        month = getDeviceMonth();
        day = getDeviceDay();
        Dialog dialog = new DatePickerDialog(this, R.style.datepicker, myEndListener, year, month, day);
        dialog.show();
        Toast.makeText(getApplicationContext(), "set end date",
                Toast.LENGTH_SHORT)
                .show();
    }

    /**
     * appends the date picker text to the button
     */
    private DatePickerDialog.OnDateSetListener myStartListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {


                    buttonStart.setText(showDate(arg1, arg2+1, arg3));
                }
            };

    /**
     * appends the date picker text to the button
     */
    private DatePickerDialog.OnDateSetListener myEndListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    buttonEnd.setText(showDate(arg1, arg2+1, arg3));
                }
            };


}