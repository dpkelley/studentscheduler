package com.example.studentscheduler;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.studentscheduler.Model.InitialValuesActivity;


import java.text.ParseException;

/**
 * Created by dkell40 on 11/5/2017.
 * android {
 compileSdkVersion 25
 buildToolsVersion "25.0.2"
 minSdkVersion 16
 targetSdkVersion 25
 */

public class CourseDetails  extends InitialValuesActivity {

    private Cursor course_Cursor;
    private Cursor course_code;
    private String courseEnd;


    private Button buttonEsmail;
    private Button buttonAssessment;
    private Button buttonMentor;
    private Button buttonEditNotes;
    private Button buttonEditSave;
    private Button course_save;
    private Button buttonStart, buttonEnd;

    private CheckBox alert_start;
    private CheckBox alert_end;

    private EditText editTextNotes;
    private String fileUri;
    private Boolean isEditable;
    private Spinner spinnerTitle, spinnerStatus;



    /**
     * From AppCompatActivity class on start, initializes variables.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_details);
        launchSubMenuBar(this);

        Bundle resources = getIntent().getExtras();
        if(resources !=null) {


            courseId = resources.getString("courseId");
            courseNameCode = resources.getString("courseNameCode");
            termId = resources.getString("termId");
            //courseEnd = resources.getString("courseEnd");
            isEditable = new Boolean(resources.getString("isEditable"));
            mentorId = "1";


        }




        final InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        final Drawable editMode = getResources().getDrawable(R.drawable.edit4);
        final Drawable staticMode = getResources().getDrawable(R.drawable.edit3);
        final Drawable fileEdit = getResources().getDrawable(R.drawable.button_w);
        final Drawable fileDone = getResources().getDrawable(R.drawable.button3);
        final Drawable saveDone = getResources().getDrawable(R.drawable.edit_save2);
        final Drawable saveIdle = getResources().getDrawable(R.drawable.edit_save2);

        course_save = (Button) findViewById(R.id.course_save);

        course_save.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateCourse();
                    }
                }
        );

        alert_start = (CheckBox) findViewById(R.id.checkBox_start);
        alert_start.setOnClickListener(this);
        alert_start.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    sqlIte.updateValue("course", "alertStart", courseId, "1" ); //table, col, id, data
                else
                    sqlIte.updateValue("course", "alertStart", courseId, "0" ); //table, col, id, data
            }
        });

        alert_end = (CheckBox) findViewById(R.id.checkBox_end);
        alert_end.setOnClickListener(this);
        alert_end.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    sqlIte.updateValue("course", "alertEnd", courseId, "1"); //table, col, id, data
                }
                else
                    sqlIte.updateValue("course", "alertEnd", courseId, "0" ); //table, col, id, data
            }
        });

        buttonAssessment = (Button) findViewById(R.id.course_assessment);
        buttonAssessment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchActivity(AssessmentList.class); }
        });

        buttonMentor = (Button) findViewById(R.id.course_mentor);
        buttonMentor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchActivity(CourseMentor.class); }
        });


        course_code = getCursor("*", "", "course_code");


        buttonEditNotes = (Button) findViewById(R.id.edit_notes);
        buttonEditNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(editTextNotes.isFocusable())) {
                    editTextNotes.setFocusableInTouchMode(true);
                    keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    buttonEditNotes.setBackground(editMode);
                    editTextNotes.setBackground(fileEdit);
                    buttonEditSave.setBackground(saveDone);
                }
            }
        });

        buttonEditSave = (Button) findViewById(R.id.edit_save);
        buttonEditSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextNotes.isFocusable()) {
                    toast("Saved");
                    keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    editTextNotes.setFocusable(false);
                    buttonEditNotes.setBackground(staticMode);
                    editTextNotes.setBackground(fileDone);
                    buttonEditSave.setBackground(saveIdle);
                    addNote(editTextNotes.getText().toString(), fileUri);
                }
            }
        });

        buttonEsmail = (Button) findViewById(R.id.sendEmail);
        buttonEsmail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mailTo(editTextNotes.getText().toString());
            }
        });


        course_Cursor = getCursor("*", " WHERE courseId="+courseId, "course", "course_code");

        spinnerTitle = (Spinner)findViewById(R.id.term_name);
        spinnerStatus = (Spinner)findViewById(R.id.status);
        buttonStart = (Button)findViewById(R.id.course_start);
        buttonEnd = (Button)findViewById(R.id.course_end);
        editTextNotes = (EditText) findViewById(R.id.notes_uri); //TODO change to EditText view when edit mode
        editTextNotes.setFocusable(false);


        setdata();

        setEditEnabled(isEditable);

    }



    /**
     * From View.OnClickListener interface onClick
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.checkBox_start:
                if (alert_start.isChecked())
                    Toast.makeText(getApplicationContext(), "Daily alert set 1 week prior of start date.", Toast.LENGTH_LONG).show();
                break;
            case R.id.checkBox_end:
                if (alert_end.isChecked()) {
                    Toast.makeText(getApplicationContext(), "Daily alert set 1 week prior of due date.", Toast.LENGTH_LONG).show();
                    checked();
                }break;

        }
    }

    /**
     * Whether an alert is checked or not
     */
    private void checked(){
        course_Cursor = getCursor("*", " WHERE courseId="+courseId, "course", "course_code");
        try {
            course_Cursor.moveToFirst();
            getCourseEndAlert(courseEnd = course_Cursor.getString(3));
        }catch(ParseException e){

        }
    }

    /**
     * From AppCompatActivity class onSaveInstanceState
     */
    @Override
    protected void onSaveInstanceState(Bundle portraitOrLandscape) {
        portraitOrLandscape.putString("isEditable", isEditable.toString());
        super.onSaveInstanceState(portraitOrLandscape);

    }

    /**
     * From AppCompatActivity class onRestoreInstanceState
     */
    @Override
    public void onRestoreInstanceState(Bundle resources ){
        if(resources !=null) {
            isEditable = new Boolean(resources.getString("isEditable"));
        }
        setEditEnabled(isEditable);
        super.onResume();
    }


    /**
     * From View.InitialValuesActivity class setEditEnabled
     */
    @Override
    public void setEditEnabled(boolean value){

        isEditable = value;
        spinnerTitle.setEnabled(value);
        spinnerStatus.setEnabled(value);
        buttonStart.setEnabled(value);
        buttonEnd.setEnabled(value);
        getVisibility(value);

    }

    /**
     * getter for button visible
     */
    private void getVisibility(boolean value){
        if(value)
            course_save.setVisibility(View.VISIBLE);
        else
            course_save.setVisibility(View.INVISIBLE);

    }



    /**
     * Updates a Course to sqlite database.
     */
    public void updateCourse() {

        try{

            validateCourse(nullCheck(spinnerTitle.getSelectedItem().toString()));
            String code = validateDuplicate(getCode(spinnerTitle.getSelectedItem().toString()), courseNameCode, "course");
            String status = nullCheck(spinnerStatus.getSelectedItem().toString());
            String notesFile = addNewNote(null, hashCode(courseId+code+1));//hash = courseId, courseCodeId, StudentId
            nullCheck(buttonStart.getText().toString());
            nullCheck(buttonEnd.getText().toString());
            String updateTermId = nullCheckTerm(termId);
            //lastKnownTerm


            //Log.d("spinnerStatus 254", spinnerStatus.getPrompt().toString());
            Log.d("UPDATE COURSE table", "course");
            Log.d("UPDATE COURSE 0", courseId.toString());
            Log.d("UPDATE COURSE 1", code);
            Log.d("UPDATE COURSE 2", buttonStart.getText().toString());
            Log.d("UPDATE COURSE 3", buttonEnd.getText().toString());
            Log.d("UPDATE COURSE 4", status);
            Log.d("UPDATE COURSE 5", mentorId);
            Log.d("UPDATE COURSE 6", getCheckBoxState(alert_start)); //alert
            Log.d("UPDATE COURSE 7", updateTermId);
            Log.d("UPDATE COURSE 8", notesFile);
            Log.d("UPDATE COURSE 9", getCheckBoxState(alert_end)); //alert

            long isUpdate = sqlIte.updateData(

                    "course",
                    courseId.toString(),
                    code,
                    buttonStart.getText().toString(),
                    buttonEnd.getText().toString(),
                    status,  //TODO FIX status insert
                    mentorId,
                    getCheckBoxState(alert_start),
                    updateTermId,
                    notesFile,
                    getCheckBoxState(alert_end)
            );

            if(isUpdate > 0) {

                Toast.makeText(CourseDetails.this, "Data Update", Toast.LENGTH_LONG).show();
                restart();
            }
            else {

                Toast.makeText(CourseDetails.this, "Data not Updated", Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            showAlert("Warning",e.getMessage());

        }
    }

    /**
     * Resets the view and repopulated the list.
     */
    public void restart(){
        launchActivity(MainNavigationPanel.class);
        launchActivity(TermList.class);
        launchActivity(CourseDetails.class);
        this.finish();
    }

    /**
     * From InitialValuesActivity class launchActivity
     */
    public void launchActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.putExtra("courseId", courseId);
        intent.putExtra("courseNameCode", courseNameCode);
        intent.putExtra("termId", termId);
        intent.putExtra("courseEnd", courseEnd);
        startActivity(intent);
    }

    /**
     * Gets text from notes and passes to intent of another application to mail out.
     */
    protected void mailTo(String notes) {
        String[] emailTo = {"dkell40@my.wgu.edu"};
        String[] emailCc = {""};
        Intent emailClient = new Intent(Intent.ACTION_SEND);

        emailClient.setData(Uri.parse("mailto:"));
        emailClient.setType("text/plain");
        emailClient.putExtra(Intent.EXTRA_EMAIL, emailTo);
        emailClient.putExtra(Intent.EXTRA_CC, emailCc);
        emailClient.putExtra(Intent.EXTRA_SUBJECT, "Course Notes for "+ spinnerTitle.getSelectedItem().toString());//course Notes for Code  or Course Assessment Code
        emailClient.putExtra(Intent.EXTRA_TEXT, notes); //notesUri

        try {
            startActivity(Intent.createChooser(emailClient, "Send mail..."));

        } catch (android.content.ActivityNotFoundException ex) {
            toast("Invalid, cannot open email client");
        }
    }

    /**
     * gets the view and repopulated the alert.
     */
    public void getCourseEndAlert(String date)  throws ParseException {
        if(course_Cursor.getString(9).equals("0"))
            return;

        java.sql.Date today = convertUtilToSql(new java.util.Date());
        java.util.Date start = dateformatter.parse("01/01/1970");
        java.util.Date alert = convertUtilToSql(dateformatter.parse("01/01/1970"));

        start = dateformatter.parse(date);
        alert = dateformatter.parse(dateMinusDays(start, 7));

        if(!today.before(alert) && today.before(start)) {
            String title = course_Cursor.getString(11)+" - "+course_Cursor.getString(10);
            String endDate = course_Cursor.getString(3);
            courseEnd  = course_Cursor.getString(3);
            String message = "This course is due by "+endDate+".";
            getAlertDialogue("Alert!", message);
        }
    }



    /**
     * sets the database data to view
     */
    private void setdata(){
        if(course_Cursor.getCount() != 0)
            while (course_Cursor.moveToNext()) {

                String[] mentorInfo = getMentor(mentorId = course_Cursor.getString(5));
                courseNameCode = course_Cursor.getString(11) + " - " + course_Cursor.getString(1);

                //spinnerTitle.setPrompt(courseNameCode);
                setSpinner(R.layout.spinner_dropdown_header, spinnerTitle, courseNameCode, getCourseTitleAndCode(course_code));

                buttonStart.append(course_Cursor.getString(2));
                buttonEnd.append(course_Cursor.getString(3));

                alert_start.setChecked(setAlertCheckbox(course_Cursor.getString(6)));
                alert_end.setChecked(setAlertCheckbox(course_Cursor.getString(9)));

                courseNameCode = course_Cursor.getString(1);

                setSpinner(R.layout.spinner_dropdown_normal, spinnerStatus, course_Cursor.getString(4), status);
                editTextNotes.setText(getNote(fileUri = course_Cursor.getString(8)));

                if(course_Cursor.getString(11).equals("ADD COURSE")) {
                    isEditable = true;
                }
                try {
                    getCourseEndAlert(courseEnd = course_Cursor.getString(3));
                }catch(ParseException e){

                }
            }
    }


    /**
     * gets the mentor data
     */
    private String[] getMentor(String id){
        String[] data = new String[5];
        Cursor mentorCursor = getCursor("*", " WHERE mentorId="+id, "mentor");
        if(mentorCursor.getCount() != 0)
            while (mentorCursor.moveToNext()) {
                for(int i = 0; i < data.length; i++)
                    data[i] = mentorCursor.getString(i);
            }
        return data;
    }

    /**
     * sets the start data
     */
    public void setStartDate(View view) {
        year = getDeviceYear();
        month = getDeviceMonth();
        day = getDeviceDay();
        Dialog dialog = new DatePickerDialog(this, R.style.datepicker, myStartListener, year, month, day);
        dialog.show();
        Toast.makeText(getApplicationContext(), "set start date",
                Toast.LENGTH_SHORT)
                .show();
    }
    /**
     * sets the end data
     */
    public void setEndDate(View view) {
        year = getDeviceYear();
        month = getDeviceMonth();
        day = getDeviceDay();
        Dialog dialog = new DatePickerDialog(this, R.style.datepicker, myEndListener, year, month, day);
        dialog.show();
        Toast.makeText(getApplicationContext(), "set end date",
                Toast.LENGTH_SHORT)
                .show();
    }

    /**
     * appends the date picker text to the button
     */
    private DatePickerDialog.OnDateSetListener myStartListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {


                    buttonStart.setText(showDate(arg1, arg2+1, arg3));
                }
            };

    /**
     * appends the date picker text to the button
     */
    private DatePickerDialog.OnDateSetListener myEndListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    buttonEnd.setText(showDate(arg1, arg2+1, arg3));
                }
            };




}
