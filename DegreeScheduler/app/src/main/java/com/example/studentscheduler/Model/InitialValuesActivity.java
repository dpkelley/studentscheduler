package com.example.studentscheduler.Model;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.studentscheduler.CourseList;
import com.example.studentscheduler.MainNavigationPanel;
import com.example.studentscheduler.TermList;
import com.example.studentscheduler.R;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by dkell40 on 9/25/2017.
 * android {
 compileSdkVersion 25
 buildToolsVersion "25.0.2"
 minSdkVersion 16
 targetSdkVersion 25
 */

public abstract class InitialValuesActivity extends AppCompatActivity implements View.OnClickListener  {

    protected static String courseId;
    protected static String courseNameCode;
    protected static String termId = "1";
    protected static String lastKnownTerm = "1";
    protected static String mentorId;
    protected static String term = "Curriculum";


    protected SQLiteManager sqlIte = new SQLiteManager(this);
    protected Calendar calendar;
    protected int year, month, day;
    protected java.util.Date date;
    protected SimpleDateFormat dateformatter = new SimpleDateFormat("MM/dd/yyyy");
    private Button buttonLaunchTermList;
    private Button buttonLaunchCourseList;
    private Button buttonFinishHome;



    /**
     * Sub menu bar is located in the course detauls, assessment list, and assessment details layouts.
     */
    public void launchSubMenuBar(final AppCompatActivity cls){
        buttonLaunchTermList = (Button) findViewById(R.id.btn_terms);
        buttonLaunchTermList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( !(cls instanceof TermList)) {
                    launchActivity(MainNavigationPanel.class);
                    launchActivity(TermList.class);
                    cls.finish();
                }

            }
        });


        buttonLaunchCourseList = (Button) findViewById(R.id.btn_courses);
        buttonLaunchCourseList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( !(cls instanceof CourseList) && !(cls instanceof MainNavigationPanel)) {
                    launchActivity(MainNavigationPanel.class);
                    launchActivity(TermList.class);
                    launchActivity(CourseList.class);
                    cls.finish();
                }

            }
        });


        buttonFinishHome = (Button) findViewById(R.id.finish_home);
        buttonFinishHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( !(cls instanceof MainNavigationPanel)) {
                    launchActivity(MainNavigationPanel.class);
                    cls.finish();
                }
            }
        });

    }

    /**
     * Launches another activity
     */
    public void launchActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.putExtra("courseId", courseId);
        intent.putExtra("courseNameCode", courseNameCode);
        intent.putExtra("termId", termId);
        startActivity(intent);
    }


    /**
     * static databse variables to choose from
     */
    public static final String[] status = {"In Progress", "Enrolled", "Unapproved", "Completed", "Dropped", "Plan To take",
            "Approved", "Activated", "Failed" //Tw Cen MT   42
    };


    /**
     * Reusable code for getting an alert message with specified header.
     */
    public void getAlertDialogue(String title, String message){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setTitle(title);

        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        //sql set pass
                        dialog.cancel();
                    }
                });

        alertDialogBuilder.setCancelable(true);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    /**
     * Sets the menu or action bar menu layout file
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    /**
     * Parses the java util date to a specified format
     */
    public java.util.Date parseDate(String date){
        java.util.Date result = null;
        try {
            result =  (java.util.Date) dateformatter.parse(date);

        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        return result;
    }

    /**
     * Handles the menu or action bar menu selections
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case R.id.course_table:
                showAlert("course",handleViewTable("course", "*", "", sqlIte.courseColuums).toString());
                return true;
            case R.id.mentor_table:
                showAlert("mentor",handleViewTable("mentor", "*", "",  sqlIte.mentorColuums).toString());
                return true;
            case R.id.assessment_table:
                showAlert("assessment",handleViewTable("assessment", "*", "",  sqlIte.assessmentColuums).toString());
                return true;
            case R.id.term_table:
                showAlert("term",handleViewTable("term", "*", "",  sqlIte.termColuums).toString());
                return true;
            case R.id.course_code_table:
                showAlert("course_code",handleViewTable("course_code", "*", "",  sqlIte.course_codeColuums).toString());
                return true;
            case R.id.assessment_code_table:
                showAlert("code",handleViewTable("code", "*", "",  sqlIte.assessment_codeColuums).toString());
                return true;
            case R.id.edit_enabled:
                setEditEnabled(true);
                return true;
            case R.id.exit:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * For subclasses
     */
    public abstract void setEditEnabled(boolean value);


    /**
     * For subclasses, reusable code to set a spinner with array data and select the item in the array with
     * the title that is given. A resource file can be set as well.
     */
    public void setSpinner(int resource, Spinner spinner, String title, String... items){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                resource, items);

        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(adapter);
        int length = adapter.getCount();
        int position = 0;
        for (int i = 0;i < length; i++) {
            Log.d("CourseDetails.class 157", adapter.getItem(i)+"");
            if (adapter.getItem(i).equals(title)) {
                position = adapter.getPosition(adapter.getItem(i));

            }
        }

        spinner.setSelection(position);
    }

    /**
     * For subclasses, reusable array data  to set a spinner with
     */
    public String[] getCourseTitleAndCode(Cursor cursor){

        String[] data = new String[cursor.getCount()];
        int i = 0;
        if(cursor.getCount() != 0)
            while (cursor.moveToNext()) {
                data[i] = cursor.getString(1) + " - " + cursor.getString(0);
                i++;
            }
        return data;
    }

    /**
     * For subclasses, reusable array data  to set a spinner with
     */
    public String[] getMentorNameAndId(Cursor cursor){

            String[] data = new String[cursor.getCount()+1];
            data[0] = "";
            int i = 1;
            if(cursor.getCount() != 0)
                while (cursor.moveToNext()) {
                    data[i] = cursor.getString(1);
                    i++;
                }
            return data;

    }

    /**
     * For subclasses, reusable array data  to set a spinner with
     */
    public String[] getAssessmentTitleAndCode(Cursor cursor){

        String[] data = new String[cursor.getCount()];
        int i = 0;
        if(cursor.getCount() != 0)
            while (cursor.moveToNext()) {
                data[i] = cursor.getString(4) + ": " + cursor.getString(1) + " - " + cursor.getString(0);
                i++;
            }
        return data;
    }



    /**
     * This is the initial vales to set in the database upon app install. if it is installed again, the data is ignored unless
     * the cache and app is completely uninstalled.
     */
    public void setInitialDBValues(){
        String initialNote = "";

        sqlIte.insertData("mentor", "1", "NEW MENTOR", "(000) 000-0000", "(000) 000-0000", "NEW MENTOR@wgu.edu", "NEW MENTOR_GROUP@wgu.edu");
        sqlIte.insertData("mentor", "2", "Course Mentor Group Software", "(877) 435-7948", "0000", "cmsoftware@wgu.edu", "cmsoftware@wgu.edu");
        sqlIte.insertData("mentor", "3", "Course Mentor Group Communication", "(877) 435-7948", "0000", "comm@wgu.edu", "comm@wgu.edu");
        sqlIte.insertData("mentor", "4", "Course Mentor Group Leadership", "(877) 435-7948", "0000", "orgleadership@wgu.edu", "orgleadership@wgu.edu");
        sqlIte.insertData("mentor", "5", "Henry Cook", "(877) 435-7948", "0000", "henry.cook@wgu.edu", "");
        sqlIte.insertData("mentor", "6", "Tammy Willcox", "(877) 435-7948", "0000", "tammy.willcox@wgu.edu", "");
        sqlIte.insertData("mentor", "7", "Melanie Koehler", "(877) 435-7948", "6007", "melanie.koehler@wgu.edu", "");
        sqlIte.insertData("mentor", "8", "Maria Schenk", "(877) 435-7948", "6007", "maria.schenk@wgu.edu", "");
        sqlIte.insertData("mentor", "9", "John C.", "(877) 435-7948", "6007", "cmdatabase@wgu.edu", "cmdatabase@wgu.edu");
        sqlIte.insertData("mentor", "10", "Cynthia Lang", "(877) 435-7948", "5266", "cmdatabase@wgu.edu", "cmsoftware@wgu.edu");
        sqlIte.insertData("mentor", "11", "Joe Barnhart", "(877) 435-7948", "4889", "Joe.Barnhart@WGU.edu", "ugcapstoneit@wgu.edu");
        sqlIte.insertData("mentor", "12", "Dave Huff", "(877) 435-7948", "1854", "Dave.Huff@WGU.edu", "ugcapstoneit@wgu.edu");
        sqlIte.insertData("mentor", "13", "Charlie Paddock", "(877) 435-7948", "1858", "Charles.Paddock@WGU.edu", "ugcapstoneit@wgu.edu");
        sqlIte.insertData("mentor", "14", "Amy Antonucci", "(877) 435-7948", "(385) 428-7197", "amy.antonucci@wgu.edu", "cmprogramming@wgu.edu");
        sqlIte.insertData("mentor", "15", "Charles Lively", "(877) 435-7948", "(801) 924-4645", "charles.lively@wgu.edu", "cmprogramming@wgu.edu");
        sqlIte.insertData("mentor", "16", "Julie Taylor", "(877) 435-7948", "(801) 924-4645", "julie.taylor@wgu.edu", "cmprogramming@wgu.edu");
        sqlIte.insertData("mentor", "17", "Julie Taylor", "(877) 435-7948", "(801) 924-4645", "julie.taylor@wgu.edu", "cmitfund1@wgu.edu");
        sqlIte.insertData("mentor", "18", "Carolyn Sher-DeCusatis", "(877) 435-7948", "7192", "carolyn.sher@wgu.edu", "cmsoftware@wgu.edu");


        //course_codeId, title, code, cu
        sqlIte.insertData("course_code", "", "ADD COURSE", "0");
        sqlIte.insertData("course_code", "C100", "Humanities", "3");
        sqlIte.insertData("course_code", "C132", "Intro to Communication", "3");
        sqlIte.insertData("course_code", "C164", "Physics", "3");
        sqlIte.insertData("course_code", "C169", "Scripting and Programming Applications", "4");
        sqlIte.insertData("course_code", "C168", "Critical Thinking and Logic", "4");
        sqlIte.insertData("course_code", "C170", "Data Management Applications", "3");
        sqlIte.insertData("course_code", "C172", "Network and Security Foundations", "4");
        sqlIte.insertData("course_code", "C175", "Data Management Foundations", "3");
        sqlIte.insertData("course_code", "C178", "Network and Security Applications", "4");
        sqlIte.insertData("course_code", "C186", "Server Administration", "4");
        sqlIte.insertData("course_code", "C179", "Business of IT Applications", "4");
        sqlIte.insertData("course_code", "C176", "Business of IT Project Management", "4");
        sqlIte.insertData("course_code", "TPV1", "Business of IT Project Management", "4");
        sqlIte.insertData("course_code", "C185", "Network Policies and Services Management", "4");
        sqlIte.insertData("course_code", "C186", "Server Administration", "4");
        sqlIte.insertData("course_code", "C188", "Software Engineering", "4");
        sqlIte.insertData("course_code", "C189", "Data Structures", "3");
        sqlIte.insertData("course_code", "C191", "Operating Systems for Programmers", "3");
        sqlIte.insertData("course_code", "C192", "Data Management for Programmers", "4");
        sqlIte.insertData("course_code", "C193", "Client Server Application Development", "3");
        sqlIte.insertData("course_code", "C195", "Software II", "6");
        sqlIte.insertData("course_code", "C196", "Mobile Application Development", "3");
        sqlIte.insertData("course_code", "C246", "Fund. of Interconnecting Network Devices", "4");
        sqlIte.insertData("course_code", "C247", "Interconnecting Network Devices", "4");
        sqlIte.insertData("course_code", "C255", "Geography", "4");
        sqlIte.insertData("course_code", "C277", "Finite Math", "3");
        sqlIte.insertData("course_code", "C299", "Designing Customized Security", "4");
        sqlIte.insertData("course_code", "C376", "Web Development Fundamentals", "3");
        sqlIte.insertData("course_code", "C393", "IT Foundations", "3");
        sqlIte.insertData("course_code", "C394", "IT Applications", "4");
        sqlIte.insertData("course_code", "C435", "Technical Writing", "3");
        sqlIte.insertData("course_code", "RHT1", "Technical Writing", "3");
        sqlIte.insertData("course_code", "C436", "Capstone", "4");
        sqlIte.insertData("course_code", "SBT1", "Capstone", "4");
        sqlIte.insertData("course_code", "C451", "Integrated Natural Science", "4");
        sqlIte.insertData("course_code", "C455", "English Composition I", "3");
        sqlIte.insertData("course_code", "C456", "English Composition II", "4");
        sqlIte.insertData("course_code", "C459", "Intro to Probability and Statistics", "4");
        sqlIte.insertData("course_code", "C480", "Networks", "4");
        sqlIte.insertData("course_code", "C482", "Software I", "6");
        sqlIte.insertData("course_code", "C484", "Organizational Behavior and Leadership", "4");
        sqlIte.insertData("course_code", "C697", "Operating Systems I", "3");
        sqlIte.insertData("course_code", "C698", "Operating Systems II", "4");
        sqlIte.insertData("course_code", "C768", "Technical Communication", "4");
        sqlIte.insertData("course_code", "C773", "User Interface Design", "4");
        sqlIte.insertData("course_code", "C777", "Web Development Applications", "3");
        sqlIte.insertData("course_code", "C779", "Web Development Foundations", "4");
        sqlIte.insertData("course_code", "C840", "Digital Forensics in Cybersecurity", "4");
        //"studentId", "studentName", "userName", "studentMentorId", "degree"



        sqlIte.insertData("code", "", "ADD ASSESSMENT", "", "", "");
        sqlIte.insertData("code", "PTJC", "Elements of Effective Communication", "70", "120", "Pre-Assessment");
        sqlIte.insertData("code", "TJC1", "Elements of Effective Communication", "66", "120", "Objective Assessment");
        sqlIte.insertData("code", "PBNC", "Organizational Behavior and Leadership (BNC1)", "42", "90", "Pre-Assessment");
        sqlIte.insertData("code", "BNC1", "Organizational Behavior and Leadership", "37", "90", "Objective Assessment");
        sqlIte.insertData("code", "PACL", "Reasoning and Problem Solving", "30", "120", "Pre-Assessment");
        sqlIte.insertData("code", "CLC1", "Reasoning and Problem Solving", "26", "120", "Objective Assessment");
        sqlIte.insertData("code", "TBP1", "English Composition I", "0", "0", "Performance Assessment");
        sqlIte.insertData("code", "IWT1", "Literature, Arts and the Humanities: Analysis and Interpretation", "0", "0", "Performance Assessment");
        sqlIte.insertData("code", "PCJC", "Introduction to Probability and Statistics", "67", "150", "Pre-Assessment");
        sqlIte.insertData("code", "CJC1", "Introduction to Probability and Statistics", "67", "150", "Objective Assessment");
        sqlIte.insertData("code", "PAG5", "Introduction to IT", "72", "180", "Pre-Assessment");
        sqlIte.insertData("code", "GSC1", "Introduction to IT", "72", "120", "Objective Assessment");
        sqlIte.insertData("code", "KEV1", "Third Party Assessment - CompTIA A+ Part 1/2", "0", "0", "Objective Assessment");
        sqlIte.insertData("code", "ORA1", "Education Without Boundaries Orientation", "0", "0", "Performance Assessment");
        sqlIte.insertData("code", "KFV1", "CompTIA - A+", "0", "0", "Objective Assessment");
        sqlIte.insertData("code", "PEUC", "Web Development Fundamentals", "70", "120", "Pre-Assessment");
        sqlIte.insertData("code", "EUC1", "Web Development Fundamentals", "69", "120", "Objective Assessment");
        sqlIte.insertData("code", "EUP1", "Project in Web Development Fundamentals", "0", "0", "Performance Assessment");
        sqlIte.insertData("code", "PBHC", "Data Management - Foundations", "65", "150", "Pre-Assessment");
        sqlIte.insertData("code", "BHC1", "Data Management - Foundations", "65", "150", "Objective Assessment");
        sqlIte.insertData("code", "PEIC", "Data Management - Applications", "65", "150", "Pre-Assessment");
        sqlIte.insertData("code", "EIC1", "Data Management - Applications", "65", "150", "Objective Assessment");
        sqlIte.insertData("code", "VHT1", "Data Management - Applications Project", "0", "0", "Performance Assessment");
        sqlIte.insertData("code", "PIAC", "Principles of Management (IAC1)", "51", "90", "Pre-Assessment");
        sqlIte.insertData("code", "IAC1", "Principles of Management", "49", "90", "Objective Assessment");
        sqlIte.insertData("code", "NFV1", "CompTIA - Network+", "0", "0", "Objective Assessment");
        sqlIte.insertData("code", "FRV5", "Networks", "65", "150", "Pre-Assessment");
        sqlIte.insertData("code", "CRV1", "Networks", "65", "150", "Objective Assessment");
        sqlIte.insertData("code", "ARV1", "Networks", "0", "0", "Performance Assessment");
        sqlIte.insertData("code", "PBGC", "Scripting and Programming - Foundations", "55", "150", "Pre-Assessment");
        sqlIte.insertData("code", "BGC1", "Scripting and Programming - Foundations", "55", "150", "Objective Assessment");
        sqlIte.insertData("code", "VGT1", "Scripting and Programming - Applications Project", "0", "0", "Performance Assessment");
        sqlIte.insertData("code", "PDQC", "Scripting and Programming - Applications (DQC1)", "63", "150", "Pre-Assessment");
        sqlIte.insertData("code", "DQC1", "Scripting and Programming - Applications", "63", "150", "Objective Assessment");
        sqlIte.insertData("code", "ELV1", "CompTIA - Security+", "0", "0", "Objective Assessment");
        sqlIte.insertData("code", "PADO", "Data Management for Programmers", "56", "150", "Pre-Assessment");
        sqlIte.insertData("code", "ADO1", "Data Management for Programmers", "56", "150", "Objective Assessment");
        sqlIte.insertData("code", "PABO", "Data Structures", "79", "180", "Pre-Assessment");
        sqlIte.insertData("code", "ABO1", "Data Structures", "79", "180", "Objective Assessment");
        sqlIte.insertData("code", "BHP1", "Data Structures", "0", "0", "Performance Assessment");
        sqlIte.insertData("code", "PAG8", "Software Engineering", "41", "150", "Pre-Assessment");
        sqlIte.insertData("code", "AAO1", "Software Engineering", "41", "150", "Objective Assessment");
        sqlIte.insertData("code", "EKV1", "CompTIA - Project+", "0", "0", "Objective Assessment");
        sqlIte.insertData("code", "PAG3", "Business of IT - Applications", "68", "150", "Pre-Assessment");
        sqlIte.insertData("code", "BYC1", "Business of IT - Applications", "74", "150", "Objective Assessment");
        sqlIte.insertData("code", "PAG7", "Operating Systems for Programmers (ACO1)", "60", "150", "Pre-Assessment");
        sqlIte.insertData("code", "ACO1", "Operating Systems for Programmers", "60", "150", "Objective Assessment");
        sqlIte.insertData("code", "FBV1", "Software I", "0", "0", "Objective Assessment");
        sqlIte.insertData("code", "PAG9", "Client Server Application Development", "55", "150", "Pre-Assessment");
        sqlIte.insertData("code", "AEO1", "Client Server Application Development", "55", "150", "Objective Assessment");
        sqlIte.insertData("code", "GZP1", "Software II - Advanced Java Concepts", "0", "0", "Performance Assessment");
        sqlIte.insertData("code", "CLP1", "Mobile Application Development", "0", "0", "Performance Assessment");
        sqlIte.insertData("code", "DDT1", "Technical Writing", "0", "0", "Performance Assessment");
        sqlIte.insertData("code", "ENT1", "IT Capstone Written Project", "0", "0", "Performance Assessment");




        //"courseId", "course_codeId", "start", "end", "status", "courseMentorId", "studentId", "termId","notesURI", "0"
        //sqlIte.insertData("course", null, "2", "start", "end", "status", "1", "1", "1", "notesURI", "0");
        sqlIte.insertData("term", "1", "Curriculum", "06/01/2016", "12/31/2017", "");
        sqlIte.insertData("term", "2", "Term1", "06/01/2016", "12/31/2016", status[3]);
        sqlIte.insertData("term", "3", "Term2", "01/01/2017", "06/30/2017", status[3]);
        sqlIte.insertData("term", "4", "Term3", "07/01/2017", "12/31/2017", status[1]);




        //SIMULATION DATA//
        //        "2", "Term1", "01/01/2017", "06/31/2017"
        initialNote = "Ed L.\n" +
                "COMMENT: 11/13/16 at 06:56 AM\n" +
                "Like * 0 Likes\n" +
                "Hi Michael. In other words, you will hard code much of the UI and have some UI components generated by your code.";
        sqlIte.insertData("course", "1","C100","06/01/2017","06/15/2016", status[3], "8", "0","2", addNewNote(initialNote, hashCode("1C1001")),"0"); //hash = courseId, courseCodeId, StudentId
        initialNote = "The good news is that you have flexibility in how you implement the alerts. Depending on your solution, these links might help: https://developer.android.com/reference/android/app/AlertDialog.Builder.html || http://www.journaldev.com/9463/android-alertdialog";
        sqlIte.insertData("course", "2","C132","06/16/2017","08/31/2016", status[3], "9", "0","2",addNewNote(initialNote, hashCode("2C1321")),"0"); //hash = courseId, courseCodeId, StudentId
        initialNote = "I haven't taken the exam, but I believe the Wiley book is sufficient to pass the exam.  You might also be interested in en.wikiversity.org/.../Exam_98-363:_Web_Development_Fundamentals - some of the links are out of date, but there may be some useful material.";
        sqlIte.insertData("course", "3","C376","09/01/2017","10/15/2016", status[3], "10", "0","2",addNewNote(initialNote, hashCode("3C3761")),"0"); //hash = courseId, courseCodeId, StudentId
        initialNote = "There are no requirements that specify navigation. So, your solution should work. When it comes to the user experience, just make sure your interface is intuitive. I hope this helps";
        sqlIte.insertData("course", "4","C179","10/16/2017","12/31/2016", status[3], "11", "0","2",addNewNote(initialNote, hashCode("4C1791")),"0"); //hash = courseId, courseCodeId, StudentId
        //"courseId", "course_codeId", "start", "end", "status", "courseMentorId", "studentId", "termId","notesURI", "0"
        initialNote = "Cynthia L.\n" +
                "COMMENT: 10/19/16 at 01:50 PM\n" +
                "Like * 0 Likes\n" +
                "Notes would be a multiline text file . It is fine to make notes a combination of multi-line text files and photos.";
        sqlIte.insertData("course", "5","C186","01/01/2017","02/15/2017", status[3], "12", "0","3",addNewNote(initialNote, hashCode("5C1861")),"0"); //hash = courseId, courseCodeId, StudentId
        initialNote = "Bryce A.\n" +
                "QUESTION: 10/20/16 at 06:29 PM\n" +
                "Like * 1 Likes\n" +
                "I am not sure what is meant by screenshots to demonstrate I created a deployment package. I'm already including the signed APK in the submission. What am I supposed to be taking screenshots of?\n" +
                "\n" +
                "Ed L.\n" +
                "COMMENT: 10/23/16 at 05:51 AM\n" +
                "Like • 1 Likes\n" +
                "Hi Bryce. In Android Studio, you can select File | Project Structure | Modules | app | Properties to display the information.";
        sqlIte.insertData("course", "6","C777","02/16/2017","03/31/2017", status[3], "13", "0","3",addNewNote(initialNote, hashCode("6C7771")),"0"); //hash = courseId, courseCodeId, StudentId
        initialNote = "Ed L.\n" +
                "COMMENT: 10/23/16 at 05:54 AM\n" +
                "Like * 0 Likes\n" +
                "Hi Craig, your storyboard will need to include each screen and menu with appropriate flow indicators (lines and arrows). You can use any of a host of editing software to create the storyboards (even Word or PowerPoint can work). I personally use Balsamiq Mockups.";
        sqlIte.insertData("course", "7","C484","04/01/2017","04/30/2017", status[3], "14", "0","3",addNewNote(initialNote, hashCode("7C4841")),"0"); //hash = courseId, courseCodeId, StudentId
        initialNote = "Ed L.\n" +
                "COMMENT: 10/23/16 at 05:58 AM\n" +
                "Like * 0 Likes\n" +
                "Hi Craig, you have some flexibility here how you implement the requirement. You will want to ensure users can add both Objective and Performance Assessments to each course along with required dates, alerts, goals, and notes.";
        sqlIte.insertData("course", "8","C170","05/16/2017","05/31/2017", status[3], "15", "0","3",addNewNote(initialNote, hashCode("8C1701")),"0"); //hash = courseId, courseCodeId, StudentId
        initialNote = "10/26/2017 - Please be advised that the requirement to be able to capture and attach photos to notes has been dropped.";
        sqlIte.insertData("course", "9","C185","06/01/2017","06/15/2017", status[8], "16", "0","3",addNewNote(initialNote, hashCode("9C1851")),"0"); //hash = courseId, courseCodeId, StudentId


        initialNote = "Samson N.\n" +
                "QUESTION: 08/02/17 at 03:58 PM\n" +
                "Like * 0 Likes\n" +
                "Does it count as two different methods if I save the notes externally as a file and save it into the database?\n" +
                "\n" +
                "Cynthia L.\n" +
                "COMMENT: 08/03/17 at 04:42 AM\n" +
                "Like • 0 Likes\n" +
                "Yes.";
        sqlIte.insertData("course", "10","C480","07/01/2017","07/15/2017", status[3], "17", "1","4",addNewNote(initialNote, hashCode("10C2471")),"0"); //hash = courseId, courseCodeId, StudentId
        initialNote = "Samson N.\n" +
                "QUESTION: 07/27/17 at 12:20 PM\n" +
                "Like * 0 Likes\n" +
                "Is there a requirement to be able to remove a course from a term? I know it sounds pretty obvious, but there's literally nothing on the requirements regarding deleting anything. There's \"Implement validation so that a term cannot be deleted if courses are assigned to it.\"\n" +
                "\n" +
                "Carolyn S.\n" +
                "COMMENT: 07/27/17 at 12:49 PM\n" +
                "Like • 0 Likes\n" +
                "It is in the introduction, though. I'd include that capability.";
        sqlIte.insertData("course", "11","C247","07/16/2017","08/31/2017", status[0], "8", "0","4",addNewNote(initialNote, hashCode("11C4801")),"0"); //hash = courseId, courseCodeId, StudentId
        initialNote = "Samson N.\n" +
                "QUESTION: 08/03/17 at 08:59 AM\n" +
                "Like * 0 Likes\n" +
                "Can someone clarify the requirement: \"• the ability to work in portrait and landscape layout\". Does it mean we have to adjust the configurations every time the orientation changes, as stated in https://developer.android.com/guide/topics/resources/runtime-changes.html?\n" +
                "\n" +
                "Carolyn S.\n" +
                "COMMENT: 08/03/17 at 10:31 AM\n" +
                "Like • 0 Likes\n" +
                "Thanks for reaching out. I had one project returned by the evaluators a long time ago requiring that. None recently, though. I hope that helps.";
        sqlIte.insertData("course", "12","C188","09/01/2017","11/22/2017", status[1], "9", "0","4",addNewNote(initialNote, hashCode("12C1881")),"1"); //hash = courseId, courseCodeId, StudentId
        initialNote = "Carolyn Sher-DeCusatis\n" +
                "Oct 31 (8 days ago)\n" +
                "\n" +
                "to Dale, Melanie \n" +
                "Hi Dale,\n" +
                " \n" +
                "Here’s a reference on sending emails in Android: https://www.tutorialspoint.com/android/android_sending_email.htm\n" +
                " \n" +
                "I hope that helps!  Feel free to contact me if you have any further questions.\n" +
                "Best,\n" +
                "Carolyn";
        sqlIte.insertData("course", "13","C193","11/23/2017","12/15/2017", status[1], "10", "1","4",addNewNote(initialNote, hashCode("13C1931")),"0"); //hash = courseId, courseCodeId, StudentId


        initialNote = "assessment notes here";


        sqlIte.insertData("assessment", "1", "13","PAG9","Not Used","12/13/2017", status[2], addNewNote(initialNote, hashCode("PAG913")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "2", "13","AEO1","Not Used","12/14/2017", status[2], addNewNote(initialNote, hashCode("AEO113")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "3", "12","PAG8","Not Used","10/13/2017", status[2], addNewNote(initialNote, hashCode("AEO112")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "4", "12","AAO1","Not Used","10/14/2017", status[2], addNewNote(initialNote, hashCode("AAO112")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "5", "11","FRV5","Not Used","08/30/2017", status[3], addNewNote(initialNote, hashCode("FRV511")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "6", "11","CRV1","Not Used","08/30/2017", status[3], addNewNote(initialNote, hashCode("CRV111")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "7", "11","ARV1","Not Used","08/30/2017", status[2], addNewNote(initialNote, hashCode("ARV111")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "8", "10","NFV1","Not Used","07/14/2017", status[3], addNewNote(initialNote, hashCode("NFV110")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "9",  "9","PIAC","Not Used","06/14/2017", status[3], addNewNote(initialNote, hashCode("PIAC9")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "10", "9","IAC1","Not Used","06/14/2017", status[8], addNewNote(initialNote, hashCode("IAC19")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "11", "8","PEIC","Not Used","05/28/2017", status[3], addNewNote(initialNote, hashCode("PEIC8")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "12", "8","EIC1","Not Used","05/29/2017", status[3], addNewNote(initialNote, hashCode("EIC18")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "13", "8","VHT1","Not Used","05/31/2017", status[3], addNewNote(initialNote, hashCode("VHT18")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "14", "7","PBNC","Not Used","04/29/2017", status[3], addNewNote(initialNote, hashCode("PBNC7")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "15", "7","BNC1","Not Used","04/29/2017", status[3], addNewNote(initialNote, hashCode("BNC17")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "16", "6","PEUC","Not Used","03/29/2017", status[3], addNewNote(initialNote, hashCode("PEUC6")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "17", "6","EUC1","Not Used","03/29/2017", status[3], addNewNote(initialNote, hashCode("EUC16")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "18", "6","EUP1","Not Used","03/29/2017", status[3], addNewNote(initialNote, hashCode("EUP16")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "19", "4","PAG3","Not Used","12/29/2016", status[3], addNewNote(initialNote, hashCode("PAG34")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "20", "4","BYC1","Not Used","12/29/2016", status[3], addNewNote(initialNote, hashCode("BYC14")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "21", "3","PEUC","Not Used","10/15/2016", status[3], addNewNote(initialNote, hashCode("PEUC3")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "22", "3","EUC1","Not Used","10/15/2016", status[3], addNewNote(initialNote, hashCode("EUC13")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "23", "2","PTJC","Not Used","08/30/2016", status[3], addNewNote(initialNote, hashCode("PTJC2")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "24", "2","TJC1","Not Used","08/30/2016", status[3], addNewNote(initialNote, hashCode("TJC12")),"0"); //hash = codeId, courseId
        sqlIte.insertData("assessment", "25", "1","ORA1","Not Used","06/15/2016", status[3], addNewNote(initialNote, hashCode("ORA11")),"0"); //hash = codeId, courseId





    }

    /**
     * Overrides object.
     */
    public String hashCode(String code){
        Integer hash = new Integer( code.hashCode());
        String result = hash.toString().substring(1,5);
        return result+ code+".txt";
    }


    /**
     * Updates a note using the second save data requirement as a file stream.
     * Rubric section C. 12 requirement
     */
    public String addNote(String notes, String fileName) {

        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(fileName, Context.MODE_PRIVATE);
            outputStream.write(notes.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }

    /**
     * Adds a new note using the second save data requirement as a file stream.
     * Rubric section C. 12 requirement
     */
    public String addNewNote(String notes, String fileName) {

        String hasData = getNote(fileName);

        if (hasData.length() > 0) {
            return fileName;
        }

        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(fileName, Context.MODE_PRIVATE);
            if (notes != null) {
                outputStream.write(notes.getBytes());
            }
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }

    /**
     * gets a note using the second save data requirement as a file stream.
     * Rubric section C. 12 requirement
     */
    public String getNote(String fileName) {
        FileInputStream inputStream;
        String result = "";
        try {
            inputStream = openFileInput(fileName);
            int content;
            while ((content = inputStream.read()) != -1) {
                // convert to char and display it
                result = result + (char) content;

            }
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Parses the substring
     */
    public String getCode(String title){
        return title.substring(title.length()-4, title.length());
    }

    /**
     * Simple selection command for the sqlite database to populate data in the app.
     */
    public Cursor getCursor(String columns, String where, String... tables){
        return sqlIte.selectItemsFromLeft(columns, where, tables);
    }

    /**
     * Gets the calendar date and parses in a specific format.
     */
    public String showDate(int year, int month, int day) {
        String calDay = parsedDate(day);
        String calMonth = parsedDate(month);
        String result = new StringBuilder().append(calMonth).append("/")
                .append(calDay).append("/").append(year).toString();
        return result;
    }

    /**
     * Parses in a specific format.
     */
    public String parsedDate(int date){
        String result = date+"";
        if(result.length() < 2)
            result = 0+result;
        return result;

    }

    /**
     * Allows to view the full database within the application menu.
     */
    public StringBuffer handleViewTable(String sqlTable, String items, String where, String[] columns){

        Cursor res = sqlIte.selectItemsFromLeft(items, where, sqlTable);
        StringBuffer buffer = new StringBuffer();

        if(res.getCount() == 0) {
            buffer.append("empty");
            return buffer;
        }


        while (res.moveToNext()) {
            for(int i = 0;i < columns.length;i++) {
                if(i == columns.length-1)
                    buffer.append(columns[i] + " :" + res.getString(i) + "\n\n");
                else
                    buffer.append(columns[i] + " :" + res.getString(i) + "\n");
            }
        }

        return buffer;
    }

    /**
     * reusable Alert builder
     */
    public void showAlert(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();

    }

    /**
     * reusable toast message
     */
    public void toast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    /**
     * calculates the last date within all terms and returns a new data for the degree plan main screen.
     */

    public java.util.Date getLastDate(Cursor termCursor){

        java.util.Date result = null;
        java.util.Date date = null;

        result = parseDate("01/01/1970");

        termCursor.moveToFirst();
        while (termCursor.moveToNext()) {


            if(termCursor.getString(3) == null || termCursor.getString(3).equals("")) continue;
            date = parseDate(termCursor.getString(3));
            if(date.after(result) )
                result = date;
        }
        result.getMonth();

        String full = result.toString();


        Log.d("Navi.class 98", "result.getDeviceMonth() "+result.getMonth());
        Log.d("Navi.class 99", "result.getDay() "+result.getDay());
        Log.d("Navi.class 100", "result.getDeviceYear() "+result.getYear());
        Log.d("Navi.class 101", "result.toString() "+result.toString().substring(4,7));

        return result;

    }

    /**
     * Used for the check box alerts within course and assessment details.
     */
    public boolean setAlertCheckbox(String alert){
        if(alert.equals("1"))
            return true;
        return false;
    }


    /**
     * Based on data sets the drawabl image in the list view.
     */
    public Drawable getDrawableStatusType(String data){
        Log.d("CourseList.class 135", data);
        Drawable result = getResources().getDrawable(R.drawable._green);

        switch (data) {
            case "Completed" : result = getResources().getDrawable(R.drawable._blue);break;
            case "Failed" : result = getResources().getDrawable(R.drawable._red);break;
            default : result = getResources().getDrawable(R.drawable._green);break;
        }
        return result;
    }

    /**
     * Since old java.util.date and java.sql.date is buggy, I created a simple month getter based on
     * the given criteria.
     */
    public String getMonth(String month){
        String i = "";
        switch (month){
            case "JAN" : return i = "1";
            case "FEB" : return i = "2";
            case "MAR" : return i = "3";
            case "APR" : return i = "4";
            case "MAY" : return i = "5";
            case "JUN" : return i = "6";
            case "JUL" : return i = "7";
            case "AUG" : return i = "8";
            case "SEP" : return i = "9";
            case "OCT" : return i = "10";
            case "NOV" : return i = "11";
            case "DEC" : return i = "12";
        }

        return i;

    }

    /**
     * Since old java.util.date and java.sql.date is not friendly, I created a simple month getter based on
     * the given criteria.
     */
    public static Integer getMonthLength(Integer month){
        Integer i = -1;
        switch (month){
            case 1 : return i = 31;
            case 2 : return i = 28;
            case 3 : return i = 31;
            case 4 : return i = 30;
            case 5 : return i = 31;
            case 6 : return i = 30;
            case 7: return i = 31;
            case 8 : return i = 31;
            case 9: return i = 30;
            case 10 : return i = 31;
            case 11 : return i = 30;
            case 12 : return i = 31;
        }

        return i;

    }

    /**
     * Since old java.util.date and java.sql.date is buggy, I created a simple parser for the android devices year
     */

    public Integer getDeviceYear(){
        java.util.Date today = Calendar.getInstance().getTime();
        return new Integer(today.toString().substring(today.toString().length()-4, today.toString().length()));
    }

    /**
     * Since old java.util.date and java.sql.date is buggy, I created a simple parser for the android devices month
     */
    public Integer getDeviceMonth(){
        java.util.Date today = Calendar.getInstance().getTime();
        return new Integer(getMonth(today.toString().substring(4,7).toUpperCase()))-1;
    }

    /**
     * Since old java.util.date and java.sql.date is buggy, I created a simple parser for the android device day
     */
    public Integer getDeviceDay(){
        java.util.Date today = Calendar.getInstance().getTime();
        return new Integer(today.toString().substring(8,10).toUpperCase());
    }

    /**
     * Java Dat helper and parser
     */
    private static String setZero(String r){
        if(r.length()< 2)
            return "0"+r;
        else
            return r;
    }

    /**
     * Java Dat helper and parser
     */
    public String getCalendarYear(java.util.Date date){
        return new Integer(date.toString().substring(date.toString().length()-4, date.toString().length())).toString();
    }

    /**
     * Java Dat helper and parser
     */
    public String getCalendarMonth(java.util.Date date){
        Integer result = new Integer(getMonth(date.toString().substring(4,7).toUpperCase()));
        return setZero(result.toString());
    }

    /**
     * Java Dat helper and parser
     */
    public String getCalendarDay(java.util.Date date){
        Integer result = new Integer(date.toString().substring(8,10).toUpperCase());
        return setZero(result.toString());
    }


    /**
     * Java Dat helper and parser
     */
    public static java.sql.Date convertUtilToSql(java.util.Date uDate) {
        java.sql.Date sDate = new java.sql.Date(uDate.getTime());
        return sDate;
    }

    /**
     * Based on given date, calculates the date minus the value in days.
     */
    public String dateMinusDays(java.util.Date date, int value){
        int offset = value-1;

        Integer result = 0;
        SimpleDateFormat dateformatter = new SimpleDateFormat("MM/dd/yyyy");

        Integer month = new Integer(getCalendarMonth(date));
        Integer day = new Integer(getCalendarDay(date));
        Integer year = new Integer(getCalendarYear(date));

        if(day < value && month != 1){
            result = (value) - day;
            result = getMonthLength(--month) - result;
            System.out.println("LINE: 1");
        }
        else if(day == value && month != 1){
            result = getMonthLength(--month);
            System.out.println("LINE: 2");
        }
        else if(day == value && month == 1){
            --year;
            month = 12;;
            result = 31;
            System.out.println("LINE: 3");
        }
        else if(day < value && month == 1){
            result = (offset) - day;
            result = 31 - result-1;
            --year;
            month = 12;;
            System.out.println("LINE: 4");
        }

        else{
            result = day - offset;
            result = getPreviousDay(result, month);
            System.out.println("LINE: 5");
        }

        String parse = setZero(month.toString())+"/"+setZero(result.toString())+"/"+year;
        return parse;
    }



    /**
     * Based on given date, calculates the date minus the value in days.
     */
    public  Integer getPreviousDay(int day, int month){
        int lastDayOfMonth = getMonthLength(month);

        if(day == 1)
            return day = lastDayOfMonth;
        else
            return --day;
    }


    /**
     * Based on given cursor, calculates the date
     */
    public String getNewDate(Cursor cursor, int increment){
        cursor.moveToFirst();

        java.util.Date last =  getLastDate(cursor);
        String full = last.toString();
        Integer year = new Integer(full.substring(full.length()-4, full.length()));
        Integer month = new Integer(getMonth(full.substring(4,7).toUpperCase()));
        month = month + increment;

        if(month > 12) {
            month = month - 12;
            year++;


        }

        String stringMonth = month.toString();

        return setZero(stringMonth)+"/01/"+year;
    }




    /**
     * getter for id
     */
    public String getNewId(String name, String id){
        switch(name){
            case "NEW MENTOR" : return null ;
            default : return id ;
        }

    }

    /**
     * if actually null, does not throw exception and prints null to console
     */
    public String printNull(String name){
        if(name == null)
            return "null";
        return name;
    }

    /**
     * Input validation
     */
    public String validateCourse(String course){
        switch(course){
            case "ADD COURSE" : throw new IllegalArgumentException("Invalid selection for course") ;
            case "ADD COURSE - " : throw new IllegalArgumentException("Invalid selection for course") ;
            case "ADD COURSE -" : throw new IllegalArgumentException("Invalid selection for course") ;
            default : return course ;
        }
    }

    /**
     * Input validation
     */
    public String nullCheck(String data){
        if (data == null || data.equals(""))
            throw new IllegalArgumentException("Data is null or Curriculum selected");
        return data;

    }
    /**
     * Input validation
     */
    public String nullCheckTerm(String data){
        if (data == null || data.equals(""))
        return data = lastKnownTerm;
        return data;

    }
    /**
     * Input validation
     */
    public String nullDate(String data){
        if (data == null || data.equals(""))
            throw new IllegalArgumentException("Due is null, please select date");
        return data;

    }

    /**
     * Input validation
     */
    public String nullMentor(String data){
        if (data == null || data.equals(""))
            throw new IllegalArgumentException("Please add new mentor name");
        return data;

    }

    /**
     * Input validation
     */
    public String validateDuplicate(String code, String original, String table) {
        Log.d("INITIAL 684", code);
        Cursor res = null;
        switch(table){
            case "course" : res = getCursor("*", " WHERE course_codeId='"+code+"'", "course");break;
            case "assessment" : res = getCursor("*", " WHERE codeId='"+code+"'", "assessment");break;
        }


        if(!(res.getCount() == 0)){
            if(code.equals(original))
                return code;
            throw new IllegalArgumentException(table +" already exists in degree plan! ");
        }else{
            return code;
        }
    }

    /**
     * Input validation
     */
    public String getCheckBoxState(CheckBox checkBox){
        if(checkBox.isChecked())
            return "1";
        else
            return "0";

    }

    /**
     * Input validation
     */
    public String validateDuplicateTerm(String title, String original) {
        Log.d("INITIAL 732", title);
        Cursor res = getCursor("*", " WHERE title='"+title+"'", "term");

        if(!(res.getCount() == 0)){
            if(title.equals(original))
                return title;
            throw new IllegalArgumentException("Cannot have duplicate titles. \n" +
                    "Please add a new term under the Degree Plan if necessary");
        }else{
            return title;
        }
    }

    /**
     * getter
     */
    public Intent getIntent(Class<?> cls) {
        return new Intent(this, cls);

    }


}

