package com.example.studentscheduler;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.studentscheduler.Components.CourseListContainer;
import com.example.studentscheduler.Components.SimpleArrayAdapter;
import com.example.studentscheduler.Model.InitialValuesActivity;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by dkell40 on 9/29/2017.
 *     compileSdkVersion 25
 buildToolsVersion "25.0.2"
 minSdkVersion 16
 targetSdkVersion 25
 */

public class CourseList extends InitialValuesActivity {



    //public static final String[] courses = new String[] {"C168 - Critical Thinking and Logic", "C172 - Network and Security Foundations", "C186 - Server Administration", "C480 - Networks"};
    private Cursor course_class_Cursor;
    private Cursor assessment_Cursor;
    private ListView listView;
    private List<CourseListContainer> values;
    private TextView a_term;
    private ScrollView scroll;
    private Button course_add;
    private Button course_subtract;
    private Button course_details;
    private long viewId = -1;


    /**
     * From AppCompatActivity class on start, initializes variables.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_list);
        launchSubMenuBar(this);
        term = "Curriculum";
        Bundle resources = getIntent().getExtras();
        if(resources !=null) {
            termId = resources.getString("termId");
            term = resources.getString("termTitle");

        }

        course_class_Cursor = getCursor("*", " WHERE termId="+termId, "course", "course_code");

        course_add = (Button) findViewById(R.id.course_add);
        course_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { addCourse(); }
        });


        course_subtract = (Button) findViewById(R.id.course_subtract);
        course_subtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { deleteCourse(); }
        });

        course_details = (Button) findViewById(R.id.course_details);
        course_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { viewId = -1;launchActivity(CourseDetails.class); }
        });

        a_term = (TextView) findViewById(R.id.a_term);
        scroll = (ScrollView) findViewById(R.id.scroll);

        scroll.setEnabled(false);
        scroll.setFocusable(false);

        a_term.setText(term);

        values = new ArrayList();

        setItems();

        listView = (ListView) findViewById(R.id.list);

        SimpleArrayAdapter adapter = new SimpleArrayAdapter(this, R.layout.layout_course_list, values);

        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CourseListContainer item = (CourseListContainer) listView.getAdapter().getItem(position);
                courseId = item.getId();

                if(viewId != id){
                    viewId = id;
                    toast("Selected " + item.getTitle());
                }else {
                    viewId = -1;
                    Intent intent = getIntent(CourseDetails.class);
                    intent.putExtra("courseId", courseId);
                    intent.putExtra("termId", termId);

                    startActivity(intent);
                }
            }
        });

    }

    /**
     * Adds items to the list adapter container for rendering.
     */
    private void setItems(){
        Drawable blue = getResources().getDrawable(R.drawable.banner_bar_blue_left);
        Drawable green = getResources().getDrawable(R.drawable.banner_bar_green_left);
        Drawable red = getResources().getDrawable(R.drawable.banner_bar_red_left);
        Drawable dropped = getResources().getDrawable(R.drawable.banner_bar_dropped_left);
        Drawable grey = getResources().getDrawable(R.drawable.banner_bar_grey_left);

        int i = 0;
        if(course_class_Cursor.getCount() != 0)
            while (course_class_Cursor.moveToNext()) {
                CourseListContainer item = new CourseListContainer();
                item.setId(courseId = course_class_Cursor.getString(0));//1 id
                item.setTitle(course_class_Cursor.getString(11) + " - " + course_class_Cursor.getString(10));//2 title

                assessment_Cursor = getCursor("*", " WHERE courseId='"+course_class_Cursor.getString(0)+"'", "assessment", "code");

                if(assessment_Cursor.getCount() != 0)
                    while (assessment_Cursor.moveToNext()) {
                        if(assessment_Cursor.getString(12).equals("Performance Assessment") ){
                            item.setPerformance(getDrawableStatusType(assessment_Cursor.getString(5)));
                        }

                        else if(assessment_Cursor.getString(12).equals("Objective Assessment") ){
                            item.setObjective(getDrawableStatusType(assessment_Cursor.getString(5)));
                        }

                    }

                item.setImage(R.drawable._grey); //2 completed, failed

                item.setCu(course_class_Cursor.getString(12)); //3 type, 'Performance..."
//"In Progress", "Enrolled", "Unapproved", "Completed", "Dropped", "Plan To take","Approved", "Activated", "Failed"


                if(course_class_Cursor.getString(4).contains("Completed"))
                    item.setDrawable(blue);
                else if(course_class_Cursor.getString(4).contains("Failed"))
                    item.setDrawable(red);
                else if(course_class_Cursor.getString(4).contains("Dropped"))
                    item.setDrawable(dropped);
                else if(course_class_Cursor.getString(1).equals(""))
                    item.setDrawable(grey);
                else
                    item.setDrawable(green);
                values.add(item);

            }

    }

    /**
     * Inserts a course row to sqlite database.
     */
    private void addCourse(){
        Cursor termCursor = getCursor("*", " WHERE termId ='"+termId+"'", "term");
        termCursor.moveToNext();
        if(termCursor.getString(1).equals("Curriculum"))
            return;

        String start = termCursor.getString(2);
        String end = termCursor.getString(3);
        String termTitle = termCursor.getString(1);
        String note = "Course dates must be between "+start +" - "+ end + " for "+ termTitle;

        Long id = sqlIte.insertData("course", null,"",start,end, status[2], "1", "0",termId.toString(),addNewNote(note, hashCode(termTitle)),"0"); //hash = termTitle

        restart();
    }

    public void restart(){
        launchActivity(MainNavigationPanel.class);
        launchActivity(TermList.class);
        launchActivity(CourseList.class);
        this.finish();
    }


    /**
     * Deletes a course row to sqlite database.
     */
    private void deleteCourse(){

        Cursor verifyStatus = sqlIte.selectItemsFromLeft("*", " WHERE courseId='"+courseId+"'", "course");
        if(verifyStatus.getCount() != 0) {
            verifyStatus.moveToNext();

            switch(verifyStatus.getString(4)){
                case "Completed" : showAlert("Invalid", "Cannot delete a Completed course"); break;
                case "In Progress" : showAlert("Invalid", "Cannot delete an In Progress course"); break;
                case "Failed" : showAlert("Invalid", "Cannot delete a Failed course"); break;
                case "Enrolled" : showAlert("Invalid", "Cannot delete an Enrolled course"); break;
                default : sqlIte.deleteRow("course", courseId); restart(); break;

            }
        }
    }

//


    /**
     * From View.OnClickListener interface onClick
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    /**
     * From View.InitialValuesActivity class setEditEnabled
     */
    @Override
    public void setEditEnabled(boolean value){

    }



}