package com.example.studentscheduler;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.studentscheduler.Components.CourseListContainer;
import com.example.studentscheduler.Components.SimpleAssessmentAdapter;
import com.example.studentscheduler.Model.InitialValuesActivity;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by dkell40 on 9/29/2017.
 * android {
 compileSdkVersion 25
 buildToolsVersion "25.0.2"
 minSdkVersion 16
 targetSdkVersion 25
 */

public class AssessmentList extends InitialValuesActivity {

    private static String courseEnd;
    private static String assessmentId;
    private Cursor assessment_Cursor;
    private ListView listView;
    private List<CourseListContainer> values;
    private Button buttonCourseDetails;
    private Button assessmentDetails;
    private Button assessment_add;
    private Button assessment_subtract;
    private Button buttonMentor;
    private long viewId = -1;
    private int pos = -1;

    /**
     * From AppCompatActivity class on start, initializes variables.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_list);
        launchSubMenuBar(this);


        Bundle resources = getIntent().getExtras();
        if(resources !=null) {

            courseId = resources.getString("courseId");
            courseNameCode = resources.getString("courseNameCode");
            termId = resources.getString("termId");
            courseEnd = resources.getString("courseEnd");


        }

        assessment_add = (Button) findViewById(R.id.assessment_add);
        assessment_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { addAssessment(); }
        });


        assessment_subtract = (Button) findViewById(R.id.assessment_subtract);
        assessment_subtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { deleteAssessment(); }
        });


        assessment_Cursor = getCursor("*", " WHERE courseId='"+courseId+"'", "assessment", "code");

        buttonCourseDetails = (Button) findViewById(R.id.course_details);
        buttonCourseDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchActivity(CourseDetails.class); }
        });

        assessmentDetails = (Button) findViewById(R.id.double_tap);
        assessmentDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pos != -1) {
                    viewId = -1;
                    CourseListContainer item = (CourseListContainer) listView.getAdapter().getItem(pos);
                    launchActivity(AssessmentDetails.class, item.getId(), item.getCode());
                }else{
                    getAlertDialogue("", "Please select an assessment from the list.");
                }
            }
        });

        buttonMentor = (Button) findViewById(R.id.course_mentor);
        buttonMentor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchActivity(CourseMentor.class); }
        });


        Drawable blue = getResources().getDrawable(R.drawable.banner_status_blue_left);
        Drawable green = getResources().getDrawable(R.drawable.banner_status_green_left);
        Drawable red = getResources().getDrawable(R.drawable.banner_status_red_left);
        Drawable blank = getResources().getDrawable(R.drawable.banner_status_blank_left);

        values = new ArrayList();

        int i = 0;

        if(assessment_Cursor.getCount() != 0)
            while (assessment_Cursor.moveToNext()) {
                CourseListContainer item = new CourseListContainer();


                item.setId(assessment_Cursor.getString(0));
                item.setTitle(assessment_Cursor.getString(9) + " - " + assessment_Cursor.getString(8));
                item.setCu(assessment_Cursor.getString(12)); //type  getType()
                item.setCode(assessment_Cursor.getString(2));

                if(assessment_Cursor.getString(2).equals("")){
                    item.setDrawable(blank); values.add(item); continue;
                }

                if(assessment_Cursor.getString(5).contains("Completed"))
                    item.setDrawable(blue);
                else if(assessment_Cursor.getString(5).contains("Failed"))
                    item.setDrawable(red);
                else
                    item.setDrawable(green);
                values.add(item);

            }



        listView = (ListView) findViewById(R.id.list);




        SimpleAssessmentAdapter adapter = new SimpleAssessmentAdapter(this, R.layout.layout_assessment_list, values);



        listView.setAdapter(adapter);  //BUG


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                CourseListContainer item = (CourseListContainer) listView.getAdapter().getItem(position);


                if(viewId != id){

                    assessmentId = item.getId();
                    Log.d("AssessmentList BUG", assessmentId);
                    pos = position;
                    viewId = id;
                    toast("Selected " + item.getTitle());
                }else {
                    viewId = -1;
                    launchActivity(AssessmentDetails.class, item.getId(), item.getCode());

                }
            }
        });



    }

    /**
     * From View.OnClickListener interface onClick
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    /**
     * From View.InitialValuesActivity class setEditEnabled
     */
    @Override
    public void setEditEnabled(boolean value){

    }

    /**
     * Overloaded, From InitialValuesActivity class launchActivity
     */
    public void launchActivity(Class<?> cls, String courseNameCode, String assessmentCode) {

        Intent intent = new Intent(this, cls);
        intent.putExtra("courseId", courseId);
        intent.putExtra("courseNameCode", courseNameCode);
        intent.putExtra("assessmentCode", assessmentCode);
        intent.putExtra("courseEnd", courseEnd);
        startActivity(intent);
    }

    /**
     * From InitialValuesActivity class launchActivity
     */
    public void launchActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.putExtra("courseId", courseId);
        intent.putExtra("courseNameCode", courseNameCode);
        intent.putExtra("termId", termId);
        intent.putExtra("courseEnd", courseEnd);
        startActivity(intent);
    }

    /**
     * Updates an assessment to sqlite database.
     */
    private void addAssessment(){
        if(courseEnd == null || courseEnd.equals("11/15/2017"))
            courseEnd = "";
        Long id = sqlIte.insertData("assessment", null , courseId,"" ,"Not Used" ,courseEnd , status[6], addNewNote("", hashCode("ASSESSMNET")),"0"); //hash = codeId, courseId
        restart();

    }

    /**
     * Deletes an assessment to sqlite database.
     */
    private void deleteAssessment(){

        Cursor verifyStatus = sqlIte.selectItemsFromLeft("*", " WHERE assessmentId='"+assessmentId+"'", "assessment");
        if(verifyStatus.getCount() != 0) {
            verifyStatus.moveToNext();

            switch(verifyStatus.getString(5)){
                case "Completed" : showAlert("Invalid", "Cannot delete a Completed assessment"); break;
                case "In Progress" : showAlert("Invalid", "Cannot delete an In Progress assessment"); break;
                case "Failed" : showAlert("Invalid", "Cannot delete a Failed assessment"); break;
                default : sqlIte.deleteRow("assessment", assessmentId);  restart(); break;

            }
        }


    }

    /**
     * Resets the view and repopulated the list.
     */
    public void restart(){
        launchActivity(MainNavigationPanel.class);
        launchActivity(TermList.class);
        launchActivity(CourseList.class);
        launchActivity(CourseDetails.class);
        launchActivity(AssessmentList.class);
        this.finish();
    }



}