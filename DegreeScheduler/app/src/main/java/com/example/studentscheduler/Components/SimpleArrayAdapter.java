package com.example.studentscheduler.Components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.studentscheduler.R;


import java.util.List;
/**
 * Created by dkell40 on 11/12/2017.
 * This class is optimized for a single list view and is designed for the course list.
 * android {
 compileSdkVersion 25
 buildToolsVersion "25.0.2"
 defaultConfig {
 applicationId "example.com.studentscheduler"
 minSdkVersion 16
 targetSdkVersion 25
 */
public class SimpleArrayAdapter extends ArrayAdapter<CourseListContainer> {
    private final Context context;
    private final List<CourseListContainer> values;
    private int resourceID;


    /**
     * Overloaded constructor
     */
    public SimpleArrayAdapter(Context context, int resourceID, List<CourseListContainer> values) {
        super(context, resourceID, values);
        this.context = context;
        this.resourceID = resourceID;
        this.values = values;
    }

    /**
     * Nested class container similar to an array list
     */
    private class ViewHolder {
        TextView statusTypeTextView;
        TextView assessmentNameTextView;
        TextView course_performance;
        TextView course_objective;



    }

    /**
     * Getter
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SimpleArrayAdapter.ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resourceID, null);
            holder = new ViewHolder();
            holder.assessmentNameTextView = (TextView) convertView.findViewById(R.id.term_name);
            holder.statusTypeTextView = (TextView) convertView.findViewById(R.id.cu_default_banner);
            holder.course_performance = (TextView) convertView.findViewById(R.id.course_performance);
            holder.course_objective = (TextView) convertView.findViewById(R.id.course_objective);


            convertView.setTag(holder);
        } else {
            holder = (SimpleArrayAdapter.ViewHolder) convertView.getTag();
        }

        CourseListContainer courseItem = (CourseListContainer) values.get(position);
        holder.assessmentNameTextView.setText(courseItem.getTitle());
        holder.statusTypeTextView.setBackground(courseItem.getDrawable());
        holder.statusTypeTextView.setText(courseItem.getCu());
        if(courseItem.getPerformance() != null) {
            holder.course_performance.setText("P");
            holder.course_performance.setBackground(courseItem.getPerformance());
        }
        if(courseItem.getObjective() != null) {
            holder.course_objective.setText("O");
            holder.course_objective.setBackground(courseItem.getObjective());
        }



        return convertView;
    }


    /**
     * Getter
     */
    @Override
    public CourseListContainer getItem(int position) {
        return values.get(position);
    }

}

