package com.example.studentscheduler.Model;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by dkell40@wgu.edu on 9/15/2017.
 * android {
 compileSdkVersion 25
 buildToolsVersion "25.0.2"
 minSdkVersion 16
 targetSdkVersion 25
 */
public class SQLiteManager extends SQLiteOpenHelper {


    public static final String DATABASE_NAME = "Term.db";
    public static final String COURSE = "course";

                                                //"courseId", "course_codeId", "start", "end", "status", "courseMentorId", "studentId", "termId","notesURI", "photosURI"
    public static final String[] courseColuums = {"courseId", "course_codeId", "start", "end", "status", "courseMentorId", "alertStart", "termId", "notesURI", "alertEnd" };
    public static final String[] mentorColuums = {"mentorId", "mentorName", "phone1", "phone2", "email", "email2"};
    public static final String[] assessmentColuums = {"assessmentId", "courseId", "codeId", "attempt", "due", "status", "notesURI", "alertDue"};
    public static final String[] termColuums = {"termId", "title", "start", "end", "status"};
    public static final String[] course_codeColuums = {"course_codeId", "title","cu"};
    public static final String[] assessment_codeColuums = {"codeId", "title", "items", "allotted","type"};

    Context context;

    public SQLiteManager(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

//"courseId", "course_codeId", "start", "end", "status", "courseMentorId", "studentId", "termId","notesURI", "photosURI"
        db.execSQL("create table " + "course" +" (" +
                "courseId INTEGER PRIMARY KEY AUTOINCREMENT," +
                "course_codeId INTEGER," +
                "start TEXT," +
                "end TEXT," +
                "status TEXT," +
                "courseMentorId INTEGER," +

                "alertStart TEXT," +
                "termId INTEGER," +

                "notesURI TEXT," +
                "alertEnd TEXT)"
				);

        db.execSQL("create table " + "mentor" +" (" +
                "mentorId INTEGER PRIMARY KEY AUTOINCREMENT," +
                "mentorName TEXT," +
                "phone1 TEXT," +
                "phone2 TEXT," +
                "email TEXT," +
                "email2 TEXT)"
				);
//{"assessmentId", "courseId", "codeId", "attempt", "due", "status", "notesURI", "photosURI"};
        db.execSQL("create table " + "assessment" +" (" +
                "assessmentId INTEGER PRIMARY KEY AUTOINCREMENT," +
                "courseId INTEGER," +
                "codeId TEXT," +
                "attempt TEXT," +
                "due TEXT," +
                "status TEXT," +
                "notesURI TEXT," +
                "alertDue TEXT)"
				);

        db.execSQL("create table " + "term" +" (" +
                "termId INTEGER PRIMARY KEY AUTOINCREMENT," +
                "title TEXT," +
                "start TEXT," +
                "end TEXT," +
                "status TEXT)"
				);

        db.execSQL("create table " + "course_code" +" (" +
                "course_codeId TEXT PRIMARY KEY ," +
                "title TEXT," +
                "cu INTEGER)"
        );

        db.execSQL("create table " + "code" +" (" +
                "codeId TEXT PRIMARY KEY ," +
                "title TEXT," +
                "items INTEGER," +
                "allotted INTEGER," +
                "type TEXT)"
        );

    }

    /**
     * From AppCompatActivity class onUpgrade, initializes variables.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ "course");
        db.execSQL("DROP TABLE IF EXISTS "+ "mentor");
        db.execSQL("DROP TABLE IF EXISTS "+ "assessment");
        db.execSQL("DROP TABLE IF EXISTS "+ "term");
        db.execSQL("DROP TABLE IF EXISTS "+ "course_code");
        db.execSQL("DROP TABLE IF EXISTS "+ "code");
        onCreate(db);
    }


    public long insertData(String table, String... data) {

        Cursor find = selectItemsFromLeft(table+"Id", " WHERE "+table+"Id"+"='"+data[0]+"'", table);
        if(find.getCount() != 0) return -1;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        int start = 0;
        if (data[0] == null)
            start = 1;


        for(int i = start; i < getColumnHeaders(table).length; i++)
            contentValues.put(getColumnHeaders(table)[i],data[i]);

        long result = db.insert(table,null ,contentValues);
        return result;
    }

    public long updateData(String table, String... data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        for(int i = 0; i < getColumnHeaders(table).length; i++)
            contentValues.put(getColumnHeaders(table)[i],data[i]);

        //String table, ContentValues values, String whereClause, String[] whereArgs
        Log.d("SQL UPDATE courseId=", getColumnHeaders(table)[0]+"");
        long result = db.update(table, contentValues, table+"Id = ?",new String[] { data[0] });


        return result;
    }

    public int updateValue(String table, String colHeader, String id, String data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

            contentValues.put(colHeader,data);

        int result = db.update(table, contentValues, table+"Id = ?",new String[] { id });


        return result;
    }



    private String[] getColumnHeaders(String table){
        switch(table){
            case "course": return courseColuums;
            case "mentor": return mentorColuums;
            case "assessment": return assessmentColuums;
            case "term": return termColuums;
            case "course_code": return course_codeColuums;
            case "code": return assessment_codeColuums;
            default : return null;

        }
    }


    public Cursor selectItemsFromLeft(String columns, String where, String... tables) {
        String query = getLeftJoinIfMultiTable(columns, tables).toString();
        query = query + where;
/*      String result = items[0];
        for(int i = 1; i < items.length; i++)
            result = result + ", "+items[i];*/
        SQLiteDatabase db = this.getWritableDatabase();
        //Cursor res = db.rawQuery("select "+columns+" from "+tables[0],null);
        //Log.d("150", query.toString());
        Cursor res = db.rawQuery(query, null);
        return res;
    }


    public Cursor selectItemsFromRight(String columns, String where, String... tables) {
        String query = getLeftJoinIfMultiTable(columns, tables).toString();
        query = query + where;
/*      String result = items[0];
        for(int i = 1; i < items.length; i++)
            result = result + ", "+items[i];*/
        SQLiteDatabase db = this.getWritableDatabase();
        //Cursor res = db.rawQuery("select "+columns+" from "+tables[0],null);
        //Log.d("150", query.toString());
        Cursor res = db.rawQuery(query, null);
        return res;
    }

    public final StringBuilder getLeftJoinIfMultiTable(final String columns, final String... joinTables){
        final StringBuilder query = new StringBuilder("SELECT "+columns+" FROM "+joinTables[0]);


        for(int i =1;i<joinTables.length;i++){
            query.append(leftJoin(joinTables[i-1], joinTables[i]));
        }

        Log.d("163", query.toString());

        return query;
    }

    public final StringBuilder getRightJoinIfMultiTable(final String columns, final String... joinTables){
        final StringBuilder query = new StringBuilder("SELECT "+columns+" FROM "+joinTables[0]);


        for(int i =1;i<joinTables.length;i++){
            query.append(leftJoin(joinTables[i-1], joinTables[i]));
        }

        Log.d("163", query.toString());

        return query;
    }

    private final String leftJoin(final String a, final String b){
        return " LEFT JOIN " + b + " ON " +a+"."+b+"ID = " +b+"."+b+"ID";
    }

    private final String rightJoin(final String a, final String b){
        return " RIGHT JOIN " + b + " ON " +a+"."+b+"ID = " +b+"."+b+"ID";
    }

    public String[][] selectDataFrom(String sqlTable, String... items){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select "+items+" from "+sqlTable,null);


        if(res.getCount() == 0) {
            return null;
        }

        String[][] data = new String[res.getCount()][];

        int j = 0;
        while (res.moveToNext()) {
            String[] buffer = new String[getColumnHeaders(sqlTable).length];

            for(int i = 0;i < getColumnHeaders(sqlTable).length;i++) {

                    buffer[i] = res.getString(i);
            }
            data[j] = buffer;
            j++;
        }
        // Show all data
        //showAlert(sqlTable,buffer.toString());
        return data;
    }




    public Integer deleteData (String courseId) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(COURSE, "courseId = ?",new String[] {courseId});
    }

    public Integer deleteRow (String table, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(table, table+"Id = ?",new String[] {id});
    }

    public Integer deleteWhere (String table, String column, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(table, column+" = ?",new String[] {value});
    }

}