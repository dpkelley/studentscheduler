package com.example.studentscheduler;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;

import com.example.studentscheduler.Components.DropDownListAdapter;
import com.example.studentscheduler.Components.DropDownListAdapter.GroupInfo;
import com.example.studentscheduler.Components.DropDownListAdapter.SubList;
import com.example.studentscheduler.Model.InitialValuesActivity;

/**
 * Created by dkell40 on 11/12/2017.
 *     compileSdkVersion 25
 buildToolsVersion "25.0.2"
 minSdkVersion 16
 targetSdkVersion 25
 */

public class TermList extends InitialValuesActivity {

    private LinkedHashMap<String, GroupInfo> subjects = new LinkedHashMap<String, GroupInfo>();
    private ArrayList<GroupInfo> courseList = new ArrayList<GroupInfo>();
    private String[][] terms;

    private String courseCount;
    private DropDownListAdapter listAdapter;
    private ExpandableListView simpleExpandableListView;
    private Button buttonLaunchTermDetails;

    private Button buttonLaunchTermAdd;
    private Button buttonLaunchTermDelete;
    private int lastExpandedPosition = -1;
    private Cursor termCursor;
    private Integer lastTerm = 0;
    private String sum = "SUM(course_code.cu) AS total";


    /**
     * From AppCompatActivity class on start, initializes variables.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_list);
        launchSubMenuBar(this);
        Bundle resources = getIntent().getExtras();
        if(resources !=null) {
            termId = resources.getString("termId");
        }

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        date = parseDate(month+"/"+day+"/"+year);


        simpleExpandableListView = (ExpandableListView) findViewById(R.id.simpleExpandableListView);

        listAdapter = new DropDownListAdapter(TermList.this, courseList);
        simpleExpandableListView.setAdapter(listAdapter);


        buttonLaunchTermDetails = (Button) findViewById(R.id.btn_launch_term_details);
        buttonLaunchTermDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchActivity(TermDetails.class); }
        });

        buttonLaunchTermAdd = (Button) findViewById(R.id.btn_term_add);
        buttonLaunchTermAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { addTerm(); }

        });

        buttonLaunchTermDelete = (Button) findViewById(R.id.btn_term_delete);
        buttonLaunchTermDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { deleteTerm(); }

        });

        getList();


        simpleExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                GroupInfo headerInfo = courseList.get(groupPosition);
                SubList detailInfo = headerInfo.getCourseList().get(childPosition);
                courseId = detailInfo.getId();
                launchActivity(CourseList.class);


                return false;
            }
        });


        simpleExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                GroupInfo headerInfo = courseList.get(groupPosition);

                term = headerInfo.getName();

                Toast.makeText(getBaseContext(), " Selected: " + headerInfo.getName(),
                        Toast.LENGTH_LONG).show();
                return false;
            }
        });

        simpleExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    simpleExpandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });


    }

    /**
     * From AppCompatActivity class onSaveInstanceState
     */
    @Override
    protected void onSaveInstanceState(Bundle portraitOrLandscape) {
        portraitOrLandscape.putString("termId", termId);
        super.onSaveInstanceState(portraitOrLandscape);

    }

    /**
     * From AppCompatActivity class onRestoreInstanceState
     */
    @Override
    public void onRestoreInstanceState(Bundle resources ){
        if(resources !=null) {
            termId = resources.getString("termId");
        }
        super.onResume();
    }

    /**
     * From View.OnClickListener interface onClick
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    /**
     * From View.InitialValuesActivity class setEditEnabled
     */
    @Override
    public void setEditEnabled(boolean value){

    }

    /**
     * getter
     */
    private void getList(){
        termCursor = getCursor("*", "", "term");
        addItems("term");
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

    }

    /**
     * From View.InitialValuesActivity class launchActivity
     */
    @Override
    public void launchActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        for (String[] row : terms)
            for (int i = 0; i < row.length; i++) {
                if (row[1].equals(term)) {
                    lastKnownTerm = termId = row[0];
                    term = row[1];
                }
            }



        Cursor total = sqlIte.selectItemsFromLeft(sum, " WHERE course.termId='"+termId+"'", "course", "course_code");
/*        Cursor all = sqlIte.selectItemsFromLeft(count, " WHERE course.termId='"+termId+"'", "course");
        String courseCount = new Integer(all.getCount()).toString();*/
        total.moveToNext();

        intent.putExtra("termId", termId);
        intent.putExtra("termTitle", term);
        intent.putExtra("totalCu", total.getString(0));


        startActivity(intent);
    }


    /**
     * getter
     * @return theCurrentDate
     */
    private String getCurrent(String start, String end, String title){
        if ((start == null || start.equals("")) && (end == null || end.equals(""))) {
            return title;
        }
        java.util.Date today = Calendar.getInstance().getTime();


        Integer todayYear = Integer.valueOf(today.toString().substring(today.toString().length()-4, today.toString().length()));
        Integer todayMonth = Integer.valueOf(getMonth(today.toString().substring(4,7).toUpperCase()));

        Integer startMonth = Integer.valueOf(start.substring(0, 2));
        Integer endMonth = Integer.valueOf(end.substring(0, 2));

        Integer startYear = Integer.valueOf(start.substring(start.length()-4, start.length()));
        Integer endYear = Integer.valueOf(end.substring(end.length()-4, end.length()));



        if(todayYear >= startYear && todayYear <= endYear){
            Log.d("TermList.class 202", true+"");
            if(todayMonth >= startMonth && todayMonth <= endMonth) {
                Log.d("TermList.class 204", true+"");
                return title + " (Current)";

            }
        }
        Log.d("TermList.class 209", false+"");
        return title;
    }


    /**
     * Populates instance variables from cursor.
     */
    private void addItems(String table){
        lastTerm = 0;
        String[][] terms = new String[termCursor.getCount()][];

        int i = 0;
        if(termCursor.getCount() != 0)

            while (termCursor.moveToNext()) {
                String finalTitle = "";
                finalTitle = getCurrent(termCursor.getString(2), termCursor.getString(3), termCursor.getString(1));

                String[] row = new String[5];
                row[0]=termCursor.getString(0);
                row[1]=finalTitle;
                row[2]=termCursor.getString(2);
                row[3]=termCursor.getString(3);
                row[4]=termCursor.getString(4);



                terms[i] = row;
                i++;


                String all = "courseId, course.course_codeId, start, end, status, courseMentorId, studentId, termId, notesURI, photosURI, course_code.course_codeId, course_code.title, course_code.cu";//13  , COUNT(course_code.cu) AS totalCUs

                String groupBy = " GROUP BY total";
                Cursor total = sqlIte.selectItemsFromLeft(sum, " WHERE course.termId='"+termCursor.getString(0)+"'", "course", "course_code");
                Cursor course = sqlIte.selectItemsFromLeft("*", " WHERE course.termId='"+termCursor.getString(0)+"'", "course", "course_code");

                courseCount = new Integer(course.getCount()).toString();
                int check = 0;
                int totalCUs = 0;
                total.moveToNext();

                while (course.moveToNext()) {


                    totalCUs = totalCUs + new Integer(course.getString(12));


                    check++;
                    String date = termCursor.getString(2).substring(0,5) + " - " + termCursor.getString(3);


                    Drawable performance = null;
                    Drawable objective = null;

                    Cursor assessment_Cursor = getCursor("*", " WHERE courseId='"+course.getString(0)+"'", "assessment", "code");
                    if(assessment_Cursor.getCount() != 0)

                        while (assessment_Cursor.moveToNext()) {
                            if(assessment_Cursor.getString(12).equals("Performance Assessment") ){
                                performance = getDrawableStatusType(assessment_Cursor.getString(5));
                            }

                            else if(assessment_Cursor.getString(12).equals("Objective Assessment") ){
                                objective = getDrawableStatusType(assessment_Cursor.getString(5));
                            }

                        }


                    addAssessmentItems(course.getString(0), course.getString(4), finalTitle, date, course.getString(11) + " - " + course.getString(1), ""+total.getString(0), course.getString(12),  termCursor.getString(4), true, performance, objective);



                }


                if(check == 0)
                    addAssessmentItems("", "", termCursor.getString(1), "",  "", "", "", termCursor.getString(4), false, null, null);

                String result = termCursor.getString(1).substring(4);
                if(!result.equals("iculum")){
                    Integer selection = new Integer(result);


                    if (selection > lastTerm)
                        lastTerm = selection;
                }
            }
        this.terms = terms;
    }

    /**
     * Clears hash values
     */
    private void removeAll(){
        courseList.clear();
        for (int i = 0; i < subjects.values().size(); i++) {
            subjects.remove(i);
        }
        subjects.values().clear();
        subjects.clear();
    }

    /**
     * Add a term to sqlite database.
     */
    private void addTerm(){
        lastTerm++;
        String start = getNewDate(termCursor, 1);
        String end = getNewDate(termCursor, 6);
        String newTerm = "Term"+lastTerm;


        Long termId = sqlIte.insertData("term", null, newTerm, start, end, status[2]);

        String note = "Course dates must be between "+start +" - "+ end + " for "+ newTerm;

        Long id = sqlIte.insertData("course", null,"",start,end, status[2], "1", "0",termId.toString(),addNewNote(note, hashCode(newTerm)),"0"); //temporary hash = term

        clearReset();
    }


    /**
     * Resets the view and repopulated the list.
     */
    private void clearReset(){
        removeAll();
        listAdapter.notifyDataSetChanged();
        simpleExpandableListView.clearChoices();
        getList();
    }

    /**
     * Deletes a term to sqlite database.
     */
    private void deleteTerm(){

        for(String[] row : terms) {

            if (row[1].equals(term)) {
                lastKnownTerm = termId = row[0];
            }
        }

        Cursor verify = sqlIte.selectItemsFromLeft("*", " WHERE termId='"+termId+"'", "course");

        if (verify.getCount() == 0) {
            if (termId.equals("1")){
                showAlert("Invalid", "Cannot delete Curriculum");
            }else{
                if(sqlIte.deleteRow("term", termId) > 0)
                    sqlIte.deleteWhere("course", "termid", termId);

            }

            lastTerm--;
            clearReset();
        }
        else if (verify.getCount() >= 1) {
            verify.moveToNext();
            if (termId.equals("1")){
                showAlert("Invalid", "Cannot delete Curriculum");
            }
            else if (verify.getString(1).equals("") || containsAdd(verify)){
                if(sqlIte.deleteRow("term", termId) > 0)
                    sqlIte.deleteWhere("course", "termid", termId);

            }else{

                showAlert("Invalid", "Cannot delete "+term+" while courses exist!");
            }


            lastTerm--;
            clearReset();
        }



        else
            showAlert("Invalid", "Cannot delete "+term+" while courses exist!");
        term = "Curriculum";
        termId = "1";
    }

    /**
     * Checks to see if title has ADD in the name
     */
    private boolean containsAdd(Cursor cursor){
        if(cursor.getCount() > 0)
            while (cursor.moveToNext()){
                if(cursor.getString(1).equals(""))
                    return true;
            }
        return false;
    }


    /**
     * Adds items to the list adapter container for rendering.
     */
    private void addAssessmentItems(String courseId, String courseStatus, String term, String date, String course, String totalCu , String cu, String termStatus, boolean setSubList, Drawable performance, Drawable objective) {


        Drawable blue = getResources().getDrawable(R.drawable.banner_bar_blue_left);
        Drawable green = getResources().getDrawable(R.drawable.banner_bar_green_left);
        Drawable red = getResources().getDrawable(R.drawable.banner_bar_red_left);
        Drawable dropped = getResources().getDrawable(R.drawable.banner_bar_dropped_left);
        Drawable grey = getResources().getDrawable(R.drawable.banner_bar_grey_left);


        GroupInfo groupInfo = subjects.get(term);
        if (groupInfo == null) {
            groupInfo = new GroupInfo();
            groupInfo.setName(term);
            groupInfo.setDate(date);
            groupInfo.setCuTotal(totalCu);
            groupInfo.setStatus(termStatus);


            subjects.put(term, groupInfo);
            courseList.add(groupInfo);
        }

        if(setSubList == true) {
            ArrayList<SubList> courseList = groupInfo.getCourseList();
            int listSize = courseList.size();

            listSize++;

            SubList subGroup = new SubList();
            if (!(course == null)) {
                subGroup.setName(course);
                subGroup.setCu(cu);
                courseList.add(subGroup);
                subGroup.setPerformance(performance);
                subGroup.setObjective(objective);
                subGroup.setId(courseId);


                if(courseStatus.contains("Completed"))
                    subGroup.setBanner(blue);
                else if(courseStatus.contains("Failed"))
                    subGroup.setBanner(red);
                else if(courseStatus.contains("Dropped"))
                    subGroup.setBanner(dropped);
                else if(courseStatus.equals(""))
                    subGroup.setBanner(grey);
                else if(course.contains("ADD COURSE"))
                    subGroup.setBanner(grey);
                else
                    subGroup.setBanner(green);




            }
            groupInfo.setCourseList(courseList);
        }
    }


}