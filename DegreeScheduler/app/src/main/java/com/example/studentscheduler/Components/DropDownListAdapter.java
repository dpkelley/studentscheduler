package com.example.studentscheduler.Components;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.example.studentscheduler.R;
import java.util.ArrayList;

/**
 * Created by dkell40 on 11/12/2017.
 * This adapter is customized for the Term list and is used for a list with sublists.
 * android {
 compileSdkVersion 25
 buildToolsVersion "25.0.2"
 defaultConfig {
 applicationId "example.com.studentscheduler"
 minSdkVersion 16
 targetSdkVersion 25
 */
public class DropDownListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<GroupInfo> terms;

    /**
     * Constructor
     */
    public DropDownListAdapter(Context context, ArrayList<GroupInfo> terms) {
        this.context = context;
        this.terms = terms;
    }

    /**
     * Getter
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<SubList> courses = terms.get(groupPosition).getCourseList();
        return courses.get(childPosition);
    }

    /**
     * Getter
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    /**
     * Getter
     */
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) { view=null;

        SubList courseSubList = (SubList) getChild(groupPosition, childPosition);
        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.child_items, null);
        }


        TextView banner = (TextView) view.findViewById(R.id.cu_default_banner);
        if(!courseSubList.getCu().equals("0"))
        banner.setText(courseSubList.getCu());

        banner.setBackground(courseSubList.getBanner());

        TextView childItem = (TextView) view.findViewById(R.id.term_name);
        childItem.setText(courseSubList.getName().trim());

        TextView course_performance = (TextView) view.findViewById(R.id.course_performance);
        if(courseSubList.getPerformance() != null) {
            course_performance.setText("P");
            course_performance.setBackground(courseSubList.getPerformance());

        }


        TextView course_objective = (TextView) view.findViewById(R.id.course_objective);
        if(courseSubList.getObjective() != null) {
            course_objective.setText("O");
            course_objective.setBackground(courseSubList.getObjective());
        }



        return view;
    }

    /**
     * Getter
     */
    @Override
    public int getChildrenCount(int groupPosition) {

        ArrayList<SubList> productList = terms.get(groupPosition).getCourseList();
        return productList.size();

    }

    /**
     * Getter
     */
    @Override
    public Object getGroup(int groupPosition) {
        return terms.get(groupPosition);
    }

    /**
     * Getter
     */
    @Override
    public int getGroupCount() {
        return terms.size();
    }

    /**
     * Getter
     */
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    /**
     * Getter
     */
    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {

        GroupInfo headerInfo = (GroupInfo) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.group_items, null);
        }

        TextView heading = (TextView) view.findViewById(R.id.heading);
        TextView ending = (TextView) view.findViewById(R.id.ending);
        heading.setText(headerInfo.getName().trim());
        if(headerInfo.getName().trim().equals("Curriculum"))
            ending.setText("");
        else
        ending.setText(headerInfo.getStatus() + "\t|\tTotal CU: " + headerInfo.getCuTotal());

        return view;
    }

    /**
     * boolean getter
     */
    @Override
    public boolean hasStableIds() {
        return true;
    }

    /**
     * boolean getter
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    /**
     * Nested class
     */
    public static class GroupInfo {

        private String name;
        private String date;
        private String status;
        private String cuTotal;




        private ArrayList<SubList> list = new ArrayList<SubList>();

        public String getStatus() {return status;}

        public void setStatus(String status) {this.status = status;}

        public String getCuTotal() {return cuTotal;}

        public void setCuTotal(String cuTotal) {this.cuTotal = cuTotal;}



        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getName() {return name;}

        public void setName(String name) {
            this.name = name;
        }

        public ArrayList<SubList> getCourseList() {
            return list;
        }

        public void setCourseList(ArrayList<SubList> courseList) {
            this.list = courseList;
        }

    }

    /**
     * Nested class
     */
    public static class SubList {

        private String name = "";
        private Drawable performance;
        private Drawable objective;
        private String cu;

        public String getId() {return id;}

        public void setId(String id) {this.id = id;}

        private String id;

        public Drawable getBanner() {return banner;}

        public void setBanner(Drawable banner) {this.banner = banner;}

        private Drawable banner;

        public String getCu() {return cu;}

        public void setCu(String cu) {this.cu = cu;}

        public Drawable getPerformance() {return performance;}

        public Drawable getObjective() {return objective;}

        public void setPerformance(Drawable performance) {this.performance = performance;}

        public void setObjective(Drawable objective) {this.objective = objective;}

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}




