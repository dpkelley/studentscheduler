package com.example.studentscheduler;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.studentscheduler.Model.InitialValuesActivity;


import java.text.ParseException;
import java.util.ArrayList;


/**
 * Created by dkell40 on 10/12/2017.
 *     compileSdkVersion 25
 buildToolsVersion "25.0.2"
 minSdkVersion 16
 targetSdkVersion 25
 */
public class MainNavigationPanel extends InitialValuesActivity {



    private TextView textViewGraduationDate;
    private Cursor termCursor;
    private Cursor courseCursor;
    private String gradDate;
    private GridLayout alert_background;
    private ImageView alert_image;
    private TextView alert;
    private ArrayList<String> alertData;

    /**
     * From AppCompatActivity class on start, initializes variables.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);  //sets layout xml
        launchSubMenuBar(this);


        alert_background = (GridLayout) findViewById(R.id.alert_background);
        alert_image = (ImageView) findViewById(R.id.alert_image);
        alert = (TextView) findViewById(R.id.alert);
        alert_image.setVisibility(View.INVISIBLE);
        calculate();


        textViewGraduationDate = (TextView) findViewById(R.id.graduation_date);

        textViewGraduationDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                calculate();
                textViewGraduationDate.setText(gradDate);
            }
        });

        textViewGraduationDate.setText(gradDate);

        try {
            getCourseStartAlert();
        }catch(ParseException e){

        }

    }
    /**
     * From View.OnClickListener interface onClick
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    /**
     * From View.InitialValuesActivity class setEditEnabled
     */
    @Override
    public void setEditEnabled(boolean value){

    }

    /**
     * Sets the alert start int he database and calculates seven days before.
     */

    public void getCourseStartAlert()  throws ParseException {
        java.sql.Date today = convertUtilToSql(new java.util.Date());
        java.util.Date start = dateformatter.parse("01/01/1970");
        //java.sql.Date sStart = convertUtilToSql(dateformatter.parse("01/01/1970"));
        java.util.Date alert = convertUtilToSql(dateformatter.parse("01/01/1970"));
        //java.sql.Date sAlert = convertUtilToSql(dateformatter.parse("01/01/1970"));

        courseCursor = getCursor("*", " WHERE alertStart='1'", "course", "course_code");

        if(courseCursor.getCount() != 0)
            while (courseCursor.moveToNext()) {
                start = dateformatter.parse(courseCursor.getString(2));
                alert = dateformatter.parse(dateMinusDays(start, 7));


                Log.d("ALERT!!!! 90", alert.toString() + " - " + start.toString()  + " - " + courseCursor.getString(0) + " - " + today);
                Log.d("ALERT!!!! 91", !today.before(alert)+" - "+today.before(start));
                if(!today.before(alert) && today.before(start)) {

                    setAlert(courseCursor);
                }
            }

    }

    /**
     * Sets the alert start int he database and calculates seven days before.
     */
    private void calculate(){
        termCursor = getCursor("*", "", "term");
        setInitialDBValues();
        String full = getLastDate(termCursor).toString();
        String year = full.substring(full.length()-4, full.length());
        gradDate = full.substring(4,7).toUpperCase()+"\n"+ year;


    }


    /**
     * Sets the alert Notification.
     */
    private void setAlert(Cursor courseCursor){

        alertData = new ArrayList();
        for (int i = 0; i < courseCursor.getColumnCount(); i++)
            alertData.add(courseCursor.getString(i));

        String title = alertData.get(11)+" - "+alertData.get(10);
        String date = alertData.get(2);
        String message = "NOTIFICATION: "+title+", starts on "+date+".";

        alert_background.setBackgroundColor(Color.parseColor("#f7941e"));
        alert_image.setVisibility(View.VISIBLE);
        alert.setText(message);

    }




}