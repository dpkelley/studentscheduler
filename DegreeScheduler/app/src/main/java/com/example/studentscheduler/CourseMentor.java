package com.example.studentscheduler;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.studentscheduler.Model.InitialValuesActivity;


/**
 * Created by dkell40 on 11/12/2017.
 *     compileSdkVersion 25
 buildToolsVersion "25.0.2"
 minSdkVersion 16
 targetSdkVersion 25
 */

public class CourseMentor  extends InitialValuesActivity {

    private Spinner mentor_title;
    private EditText mentor_phone1;
    private EditText mentor_phone2;
    private EditText mentor_email1;
    private EditText mentor_email2;
    private EditText new_mentor_name;
    private Cursor courseMentorCursor;
    private Cursor mentorCursor;
    private Button buttonCourseDetails;
    private Button buttonAssessment;
    private Boolean isEditable;
    private Button course_save;

    /**
     * From AppCompatActivity class on start, initializes variables.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_mentor);
        launchSubMenuBar(this);

        Bundle resources = getIntent().getExtras();
        if (resources != null) {
            courseId = resources.getString("courseId");
            courseNameCode = resources.getString("courseNameCode");
            termId = resources.getString("termId");
            isEditable = new Boolean(resources.getString("isEditable"));
        }

        final InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        buttonCourseDetails = (Button) findViewById(R.id.course_details);
        buttonCourseDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchActivity(CourseDetails.class); }
        });


        buttonAssessment = (Button) findViewById(R.id.course_assessment);
        buttonAssessment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchActivity(AssessmentList.class); }
        });

        mentor_title = (Spinner) findViewById(R.id.mentor_title);


        mentor_phone1 = (EditText) findViewById(R.id.mentor_phone1);
        new_mentor_name = (EditText) findViewById(R.id.new_mentor_name);
        mentor_phone2 = (EditText) findViewById(R.id.mentor_phone2);
        mentor_email1 = (EditText) findViewById(R.id.mentor_email1);
        mentor_email2 = (EditText) findViewById(R.id.mentor_email2);

        course_save = (Button) findViewById(R.id.mentor_save);

        course_save.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateCourseAndMentor();
                    }
                }
        );
        setListeners();
        setdata();
        setEditEnabled(isEditable);

    }

    /**
     * Sets event like (click, swipe and so on) C. 12 requirement!
     */
    private void setListeners(){
        final InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        mentor_phone1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(mentor_phone1.isFocusable())) {
                    mentor_phone1.setFocusableInTouchMode(true);
                    keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            }
        });
        mentor_phone2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(mentor_phone2.isFocusable())) {
                    mentor_phone2.setFocusableInTouchMode(true);
                    keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            }
        });
        mentor_email1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(mentor_email1.isFocusable())) {
                    mentor_email1.setFocusableInTouchMode(true);
                    keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            }
        });
        mentor_email2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(mentor_email2.isFocusable())) {
                    mentor_email2.setFocusableInTouchMode(true);
                    keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            }
        });

        new_mentor_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(new_mentor_name.isFocusable())) {
                    new_mentor_name.setFocusableInTouchMode(true);
                    keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            }
        });

        mentor_title.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                Cursor newSelection = getCursor("*", " WHERE mentor.mentorName ='"+mentor_title.getSelectedItem().toString()+"'", "mentor");
                setItems(newSelection);

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing, just another required interface callback
            }



        });
    }

    /**
     * From View.InitialValuesActivity class setEditEnabled
     */
    @Override
    public void setEditEnabled(boolean value){
        String grey = "#F6F5EF";
        isEditable = value;
        mentor_title.setEnabled(value);
        mentor_phone1.setEnabled(value);
        mentor_phone2.setEnabled(value);
        mentor_email1.setEnabled(value);
        mentor_email2.setEnabled(value);
        new_mentor_name.setEnabled(value);

        mentor_phone1.setFocusable(value);mentor_phone1.setFocusableInTouchMode(value);
        mentor_phone2.setFocusable(value);mentor_phone2.setFocusableInTouchMode(value);
        mentor_email1.setFocusable(value);mentor_email1.setFocusableInTouchMode(value);
        mentor_email2.setFocusable(value);mentor_email2.setFocusableInTouchMode(value);
        new_mentor_name.setFocusable(value);new_mentor_name.setFocusableInTouchMode(value);


        if(value){
            mentor_phone1.setBackgroundColor(Color.parseColor(grey));
            mentor_phone2.setBackgroundColor(Color.parseColor(grey));
            mentor_email1.setBackgroundColor(Color.parseColor(grey));
            mentor_email2.setBackgroundColor(Color.parseColor(grey));
            new_mentor_name.setBackgroundColor(Color.parseColor(grey));
        }


        getVisibility(value);

    }

    /**
     * getter for button visible
     */
    private void getVisibility(boolean value){
        if(value) {
            course_save.setVisibility(View.VISIBLE);
            course_save.setText("Save Mentor");
        }
        else {
            course_save.setVisibility(View.INVISIBLE);
            course_save.setText("Save Mentor");
        }

    }

    /**
     * setter for cursor in database to layout views
     */
    private void setdata(){
        courseMentorCursor = getCursor("*", " WHERE mentor.mentorId ='"+mentorId+"'", "mentor");
        mentorCursor = getCursor("*", "", "mentor");
        //Log.d("CourseMentor.class 162", courseMentorCursor.getCount()+"");
        if(courseMentorCursor.getCount() != 0) {
            courseMentorCursor.moveToNext();

            if(courseMentorCursor.getString(1).equals("NEW MENTOR")) {
                isEditable = true;
            }

            String title = courseMentorCursor.getString(1);

            setSpinner(R.layout.spinner_dropdown_header, mentor_title, title, getMentorNameAndId(mentorCursor));

            mentor_phone1.setText(courseMentorCursor.getString(2));
            mentor_phone2.setText(courseMentorCursor.getString(3));
            mentor_email1.setText(courseMentorCursor.getString(4));
            mentor_email2.setText(courseMentorCursor.getString(5));


        }
    }

    /**
     * setter for cursor in database to layout views
     */
    private void setItems(Cursor cursor){
        if(cursor.getCount() != 0) {
            cursor.moveToNext();
            mentorId = cursor.getString(0);
            mentor_phone1.setText(cursor.getString(2));
            mentor_phone2.setText(cursor.getString(3));
            mentor_email1.setText(cursor.getString(4));
            mentor_email2.setText(cursor.getString(5));
        }
    }

    /**
     * Updates a mentor row to sqlite database.
     */
    private void updateCourseAndMentor(){

        try{

            String mentor = mentor_title.getSelectedItem().toString();
            mentorId = getNewId(mentor, mentorId);




            Log.d("UPDATE COURSE table", "assessment");
            Log.d("UPDATE COURSE 0", printNull(mentorId));
            Log.d("UPDATE COURSE 1", mentor);
            Log.d("UPDATE COURSE 2", mentor_phone1.getText().toString());
            Log.d("UPDATE COURSE 3", mentor_phone2.getText().toString());
            Log.d("UPDATE COURSE 4", mentor_email1.getText().toString());
            Log.d("UPDATE COURSE 5", mentor_email2.getText().toString());

            long mentorUpdated;

            if(mentorId == null) {
                mentorUpdated = insert(nullMentor(new_mentor_name.getText().toString()));
            }
            else {
                mentorUpdated = new Long(mentorId);
                update(mentor);

            }

            Log.d("NEW MENTOR ID", mentorUpdated+"");

            String updated = new Long(mentorUpdated).toString();

            mentorId = updated;

            int courseUpdated = sqlIte.updateValue(
                    "course", "courseMentorId", courseId, mentorId
            );




            if(mentorUpdated > 0) {
                Log.d("UPDATE COURSE updated", mentorUpdated+"");
                Toast.makeText(CourseMentor.this, "Data Update", Toast.LENGTH_LONG).show();
                restart();
            }
            else {
                Log.d("UPDATE COURSE failed", mentorUpdated+"");
                Toast.makeText(CourseMentor.this, "Data not Updated", Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            showAlert("Warning",e.getMessage());

        }



    }

    /**
     * Inserts a mentor row to sqlite database.
     */
    public long insert( String mentor){
        return sqlIte.insertData(

                "mentor",
                mentorId,
                mentor,
                mentor_phone1.getText().toString(),
                mentor_phone2.getText().toString(),
                mentor_email1.getText().toString(),
                mentor_email2.getText().toString()
        );

    }

    /**
     * Updates a mentor row to sqlite database.
     */
    public long update( String mentor){
        return sqlIte.updateData(

                "mentor",
                mentorId,
                mentor,
                mentor_phone1.getText().toString(),
                mentor_phone2.getText().toString(),
                mentor_email1.getText().toString(),
                mentor_email2.getText().toString()
        );
    }


    /**
     * Resets the view and repopulated the list.
     */
    public void restart(){
        launchActivity(MainNavigationPanel.class);
        launchActivity(TermList.class);
        launchActivity(CourseDetails.class);
        launchActivity(CourseMentor.class);

        this.finish();
    }

    /**
     * From View.OnClickListener interface onClick
     */
    @Override
    public void onClick(View v) {

    }
}
