package com.example.studentscheduler.Components;

import android.graphics.drawable.Drawable;

/**
 * Created by dkell40 on 11/7/2017.
 * Like an object array list but can be used publicly in a protected manner.
 * android {
 compileSdkVersion 25
 buildToolsVersion "25.0.2"
 defaultConfig {
 applicationId "example.com.studentscheduler"
 minSdkVersion 16
 targetSdkVersion 25
 */
public class CourseListContainer {
    private String id;
    private int image;
    private Drawable drawable;
    private String title;
    private String status;
    private String cu;
    private String code;



    private Drawable performance;

    private Drawable objective;

    public Drawable getPerformance() {return performance;}

    public Drawable getObjective() {return objective;}

    public void setPerformance(Drawable performance) {this.performance = performance;}

    public void setObjective(Drawable objective) {this.objective = objective;}

    public String getCode() {return code;}

    public void setCode(String code) {this.code = code;}

    public String getId() {return id;}

    public void setId(String id) {this.id = id;}

    public String getCu() {return cu;}

    public void setCu(String cu) {this.cu = cu;}

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

}