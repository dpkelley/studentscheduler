package com.example.studentscheduler;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.studentscheduler.Model.InitialValuesActivity;


import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by dkell40 on 11/5/2017.
 *     compileSdkVersion 25
 buildToolsVersion "25.0.2"
 minSdkVersion 16
 targetSdkVersion 25
 */

public class AssessmentDetails extends InitialValuesActivity {

    private final Context context = this;
    private Button course_save;
    private String assessmentId;
    private String assessmentCode;
    private String courseEnd;
    private String assessmentEnd;
    private Cursor assessment_Cursor;
    private Cursor assessment_code;
    private String fileUri;
    private ArrayList<TextView> textviews;
    private EditText editTextNotes;
    private Button buttonEditNotes;
    private Button buttonEditSave;
    private Button buttonEsmail;
    private Button buttonCourseDetails;
    private Button buttonAssessment;
    private Button buttonMentor;
    private Button buttonEnd;
    private Boolean isEditable;
    private CheckBox alert_end;
    private Spinner spinnerTitle, spinnerStatus;

    /**
     * From AppCompatActivity class on start, initializes variables.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_details);
        launchSubMenuBar(this);

        Bundle resources = getIntent().getExtras();
        if(resources !=null) {
            courseId = resources.getString("courseId");
            courseNameCode = resources.getString("courseNameCode");
            termId = resources.getString("termId");
            courseEnd = resources.getString("courseEnd");
            assessmentCode = resources.getString("assessmentCode");
            isEditable = new Boolean(resources.getString("isEditable"));

        }

        final InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        final Drawable editMode = getResources().getDrawable(R.drawable.edit4);
        final Drawable staticMode = getResources().getDrawable(R.drawable.edit3);
        final Drawable fileEdit = getResources().getDrawable(R.drawable.button_w);
        final Drawable fileDone = getResources().getDrawable(R.drawable.button3);
        final Drawable saveDone = getResources().getDrawable(R.drawable.edit_save2);
        final Drawable saveIdle = getResources().getDrawable(R.drawable.edit_save2);


        buttonAssessment = (Button) findViewById(R.id.course_assessment);
        buttonAssessment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchActivity(AssessmentList.class); }
        });



        buttonCourseDetails = (Button) findViewById(R.id.course_details);
        buttonCourseDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchActivity(CourseDetails.class); }
        });

        assessment_code = getCursor("*", "", "code");
        //buttonAssessmentComplete = (Button) findViewById(R.id.assessment_complete);
        //setCompleteAssessmentDialog();
        buttonMentor = (Button) findViewById(R.id.course_mentor);
        buttonMentor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchActivity(CourseMentor.class); }
        });

        buttonEditNotes = (Button) findViewById(R.id.edit_notes);
        buttonEditNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(editTextNotes.isFocusable())) {
                    editTextNotes.setFocusableInTouchMode(true);
                    keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    buttonEditNotes.setBackground(editMode);
                    editTextNotes.setBackground(fileEdit);
                    buttonEditSave.setBackground(saveDone);
                }
            }
        });

        buttonEditSave = (Button) findViewById(R.id.edit_save);
        buttonEditSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextNotes.isFocusable()) {
                    toast("Saved");
                    keyboard.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    editTextNotes.setFocusable(false);
                    buttonEditNotes.setBackground(staticMode);
                    editTextNotes.setBackground(fileDone);
                    buttonEditSave.setBackground(saveIdle);
                    addNote(editTextNotes.getText().toString(), fileUri);
                }
            }
        });

        buttonEsmail = (Button) findViewById(R.id.sendEmail);
        buttonEsmail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mailTo(editTextNotes.getText().toString());
            }
        });

        course_save = (Button) findViewById(R.id.assessment_complete);

        course_save.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateAssessment();
                    }
                }
        );

        alert_end = (CheckBox) findViewById(R.id.checkBox_end);
        alert_end.setOnClickListener(this);
        alert_end.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    sqlIte.updateValue("assessment", "alertDue", assessmentId, "1"); //table, col, id, data
                }
                else
                    sqlIte.updateValue("assessment", "alertDue", assessmentId, "0" ); //table, col, id, data
            }
        });


        textviews = new ArrayList<>();

        spinnerTitle = (Spinner)findViewById(R.id.assessment_type_title);textviews.add(null); //0
        spinnerStatus = (Spinner)findViewById(R.id.status);textviews.add(null); //1
        textviews.add((TextView) findViewById(R.id.time_allotted)); //2
        textviews.add((TextView) findViewById(R.id.number_items)); //3
        textviews.add((TextView) findViewById(R.id.assessment_code)); //4
        buttonEnd = (Button)findViewById(R.id.assessment_end);textviews.add(null); //5
        textviews.add((TextView) findViewById(R.id.pass_fail)); //6
        textviews.add((TextView) findViewById(R.id.status_type)); //7
        textviews.add((TextView) findViewById(R.id.pass_fail_icon)); //8




        editTextNotes = (EditText) findViewById(R.id.notes_uri); //TODO change to EditText view when edit mode
        editTextNotes.setFocusable(false);


        assessment_Cursor = getCursor("*", " WHERE assessment.codeId='"+assessmentCode+"'", "assessment", "code");

        setdata();

        setEditEnabled(isEditable);
    }

    /**
     * From View.InitialValuesActivity class setEditEnabled
     */
    @Override
    public void setEditEnabled(boolean value){

        isEditable = value;
        spinnerTitle.setEnabled(value);
        buttonEnd.setEnabled(value);
        spinnerStatus.setEnabled(value);
        getVisibility(value);

    }

    /**
     * getter for button visible
     */
    private void getVisibility(boolean value){
        if(value) {
            course_save.setVisibility(View.VISIBLE);
            course_save.setText("Save Assessment");
        }
        else {
            course_save.setVisibility(View.INVISIBLE);
            course_save.setText("Save Assessment");
        }

    }

    /**
     * From View.OnClickListener interface onClick
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.checkBox_end:
                if (alert_end.isChecked()) {
                    Toast.makeText(getApplicationContext(), "Daily alert set 1 week prior of due date.", Toast.LENGTH_LONG).show();
                    checked();
                }break;

        }
    }

    /**
     * From View.OnClickListener interface onClick
     */
    private void checked(){
        assessment_Cursor = getCursor("*", " WHERE assessment.codeId='"+assessmentCode+"'", "assessment", "code");
        try {
            assessment_Cursor.moveToFirst();
            getCourseEndAlert(assessmentEnd = assessment_Cursor.getString(4));
        }catch(ParseException e){

        }
    }

    /**
     * Getter
     */
    private String getType(String data){
        String result = "O";
        switch(data){
            case "Performance Assessment" : result = "P"; break;
            default : result = "O"; break;
        }
        return result;
    }


    /**
     * From InitialValuesActivity class launchActivity
     */
    public void launchActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        intent.putExtra("courseId", courseId);
        intent.putExtra("courseNameCode", courseNameCode);
        intent.putExtra("termId", termId);
        startActivity(intent);
    }

    protected void mailTo(String notes) {
        String[] emailTo = {"dkell40@my.wgu.edu"};
        String[] emailCc = {""};
        Intent emailClient = new Intent(Intent.ACTION_SEND);

        emailClient.setData(Uri.parse("mailto:"));
        emailClient.setType("text/plain");
        emailClient.putExtra(Intent.EXTRA_EMAIL, emailTo);
        emailClient.putExtra(Intent.EXTRA_CC, emailCc);
        emailClient.putExtra(Intent.EXTRA_SUBJECT, "Assessment Notes for "+ assessmentCode);//course Notes for Code  or Course Assessment Code
        emailClient.putExtra(Intent.EXTRA_TEXT, notes); //notesUri

        try {
            startActivity(Intent.createChooser(emailClient, "Send mail..."));

        } catch (android.content.ActivityNotFoundException ex) {
            toast("Invalid, cannot open email client");
        }
    }

    /**
     * getter for another field from a separate derived field
     */
    private String getStatus(String data){
        textviews.get(8).setBackground(getDrawableAttempt(data));
        String result = "Not Attempted";
        switch(data){
            case "Completed" : result = "Pass"; break;
            case "Failed" : result = "Fail"; break;
            case "In Progress" : result = "In Progress"; break;
            default : result = "Not Attempted"; break;

        }
        return result;
    }


    /**
     * Based on data, a drawable images is set
     */
    private Drawable getDrawableAttempt(String data){

        textviews.get(7).setBackground(getDrawableStatusType(data));

        Drawable result = getResources().getDrawable(R.drawable.not_attempted);
        switch (data) {
            case "Completed" : result = getResources().getDrawable(R.drawable.pass);break;
            case "Failed" : result = getResources().getDrawable(R.drawable.fail);break;
        }
        return result;
    }



    /**
     * Based on data, a button is set
     */
    private String setButtonCompleteType(String data){


        Drawable yellow = getResources().getDrawable(R.drawable.button_yellow);
        switch(data){
            case "Pre-Assessment" : ; yellow = getResources().getDrawable(R.drawable.button_yellow); //buttonAssessmentComplete.append("TAKE NOW");break;
            case "Objective Assessment" : ; yellow = getResources().getDrawable(R.drawable.button4); //buttonAssessmentComplete.append("ASSESSMENT CENTER");  break;
            case "Performance Assessment" : ; yellow = getResources().getDrawable(R.drawable.button_yellow);//buttonAssessmentComplete.append("TASKSTREAM");break;
        }
        //buttonAssessmentComplete.setBackground(yellow);
        return data;
    }


    /**
     * find only one match and not all occurances
     */
    private void setdata(){
        if(assessment_Cursor.getCount() != 0) {
            assessment_Cursor.moveToNext();

            assessmentId = assessment_Cursor.getString(0);

            String title = assessment_Cursor.getString(12) + ": " + assessment_Cursor.getString(9) + " - " + assessment_Cursor.getString(2);

            setSpinner(R.layout.spinner_dropdown_header, spinnerTitle, title, getAssessmentTitleAndCode(assessment_code));

            setSpinner(R.layout.spinner_dropdown_normal, spinnerStatus, assessment_Cursor.getString(5), status);

            if(assessment_Cursor.getString(9).equals("ADD ASSESSMENT")) {
                isEditable = true;
            }
            textviews.get(2).append(assessment_Cursor.getString(11));//time_allotted
            textviews.get(3).append(assessment_Cursor.getString(10));//number_items
            textviews.get(4).append(assessment_Cursor.getString(2));//assessment_code
            if(assessment_Cursor.getString(4) != null)
                buttonEnd.append(assessment_Cursor.getString(4));//textviews.get(5).append(assessment_Cursor.getString(4));//assessment_end
            if(assessment_Cursor.getString(2).equals(""))
            textviews.get(6).append("Not Attempted");//pass_fail
            else
            textviews.get(6).append(getStatus(assessment_Cursor.getString(5)));//pass_fail

            if(assessment_Cursor.getString(2).equals(""))
            textviews.get(7).append("");//pass_fail
            else
            textviews.get(7).append(getType(setButtonCompleteType(assessment_Cursor.getString(12))));//pass_fail

            alert_end.setChecked(setAlertCheckbox(assessment_Cursor.getString(7)));


            editTextNotes.setText(getNote(fileUri = assessment_Cursor.getString(8)));



        }
    }


    /**
     * Updates a Assessment row to sqlite database.
     */
    public void updateAssessment() {

        try{

            validateCourse(nullCheck(spinnerTitle.getSelectedItem().toString()));
            String code = validateDuplicate(getCode(spinnerTitle.getSelectedItem().toString()), assessmentCode, "assessment");
            String status = nullCheck(spinnerStatus.getSelectedItem().toString());
            String notesFile = addNewNote(null, hashCode(assessmentId+courseId));//hash = codeId, courseId
            nullDate(buttonEnd.getText().toString());


            //Log.d("spinnerStatus 254", spinnerStatus.getPrompt().toString());
            Log.d("UPDATE COURSE table", "assessment");
            Log.d("UPDATE COURSE 0", assessmentId);
            Log.d("UPDATE COURSE 1", courseId);
            Log.d("UPDATE COURSE 2", code);
            Log.d("UPDATE COURSE 3", "Not Used");
            Log.d("UPDATE COURSE 4", buttonEnd.getText().toString());
            Log.d("UPDATE COURSE 5", status);
            Log.d("UPDATE COURSE 6", notesFile);
            Log.d("UPDATE COURSE 7", getCheckBoxState(alert_end)); //alert

            long isUpdate = sqlIte.updateData(

                    "assessment",
                    assessmentId,
                    courseId,
                    code,
                    "Not Used",
                    buttonEnd.getText().toString(),
                    status,
                    notesFile,
                    getCheckBoxState(alert_end)
            );

            if(isUpdate > 0) {
                Log.d("UPDATE COURSE updated", isUpdate+"");
                Toast.makeText(AssessmentDetails.this, "Data Update", Toast.LENGTH_LONG).show();
                restart();
            }
            else {
                Log.d("UPDATE COURSE failed", isUpdate + "");
                Toast.makeText(AssessmentDetails.this, "Data not Updated", Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            showAlert("Warning",e.getMessage());

        }
    }

    /**
     * Resets the view and repopulated the list.
     */
    public void restart(){
        launchActivity(MainNavigationPanel.class);
        launchActivity(TermList.class);
        launchActivity(CourseDetails.class);
        launchActivity(AssessmentList.class);

        this.finish();
    }




    /**
     * setter
     */
    public void setEndDate(View view) {
        year = getDeviceYear();
        month = getDeviceMonth();
        day = getDeviceDay();
        Dialog dialog = new DatePickerDialog(this, R.style.datepicker, myEndListener, year, month, day);
        dialog.show();
        Toast.makeText(getApplicationContext(), "set end date",
                Toast.LENGTH_SHORT)
                .show();
    }

    /**
     * appends the date picker text to the button
     */
    private DatePickerDialog.OnDateSetListener myEndListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    buttonEnd.setText(showDate(arg1, arg2+1, arg3));
                }
            };


    /**
     * Based on data, a drawable images is set
     */
    @Override
    public Drawable getDrawableStatusType(String data){
        Log.d("CourseList.class 135", data);
        Drawable result = getResources().getDrawable(R.drawable.attempt_green);

        switch (data) {
            case "Completed" : result = getResources().getDrawable(R.drawable.attempt_blue);break;
            default : result = getResources().getDrawable(R.drawable.attempt_green);break;
        }
        return result;
    }


    /**
     * Getter for alerts or notifications.
     */
    public void getCourseEndAlert(String date)  throws ParseException {
        if(assessment_Cursor.getString(7).equals("0"))
            return;

        java.sql.Date today = convertUtilToSql(new java.util.Date());
        java.util.Date start = dateformatter.parse("01/01/1970");
        java.util.Date alert = convertUtilToSql(dateformatter.parse("01/01/1970"));

        start = dateformatter.parse(date);
        alert = dateformatter.parse(dateMinusDays(start, 7));
        //Log.d("ALERT!!!! 90", alert.toString() + " - " + start.toString()  + " - " + courseCursor.getString(0) + " - " + today);
        Log.d("ALERT END!!!! 343", !today.before(alert)+" - "+today.before(start));
        if(!today.before(alert) && today.before(start)) {
            String title = assessment_Cursor.getString(9)+" - "+assessment_Cursor.getString(8);
            String endDate = assessment_Cursor.getString(4);
            assessmentEnd  = assessment_Cursor.getString(4);
            String message = "This assessment is due by "+endDate+".";
            getAlertDialogue("Alert!", message);
        }
    }




}
