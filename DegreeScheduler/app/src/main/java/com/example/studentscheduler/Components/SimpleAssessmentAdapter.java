package com.example.studentscheduler.Components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.studentscheduler.R;


import java.util.List;
/**
 * Created by dkell40 on 11/12/2017.
 *  * This class is optimized for a single list view and is designed for the assessment list.
 *  android {
 compileSdkVersion 25
 buildToolsVersion "25.0.2"
 defaultConfig {
 applicationId "example.com.studentscheduler"
 minSdkVersion 16
 targetSdkVersion 25
 */
public class SimpleAssessmentAdapter extends ArrayAdapter<CourseListContainer> {
    private final Context context;
    private final List<CourseListContainer> values;
    private int resourceID;


    /**
     * Overloaded Constructor
     */
    public SimpleAssessmentAdapter(Context context, int resourceID, List<CourseListContainer> values) {
        super(context, resourceID, values);
        this.context = context;
        this.resourceID = resourceID;
        this.values = values;
    }

    /**
     * Nested class container
     */
    private class ViewHolder {
        TextView bannerTextView;
        TextView courseNameTextView;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SimpleAssessmentAdapter.ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resourceID, null);
            holder = new ViewHolder();
            holder.courseNameTextView = (TextView) convertView.findViewById(R.id.term_name);
            holder.bannerTextView = (TextView) convertView.findViewById(R.id.attempt_status_type);
            convertView.setTag(holder);
        } else {
            holder = (SimpleAssessmentAdapter.ViewHolder) convertView.getTag();
        }

        CourseListContainer courseItem = (CourseListContainer) values.get(position);

        holder.courseNameTextView.setText(courseItem.getCu() +": "+courseItem.getTitle());
        holder.bannerTextView.setBackground(courseItem.getDrawable());


        if(courseItem.getCode().equals("")){
            holder.bannerTextView.setText("");
        }else {
            holder.bannerTextView.setText(getType(courseItem.getCu())); //check Pre-Assessment and convert to O
        }
            return convertView;
    }

    /**
     * Gets the letter to be set on the banner based on assessment type.
     */
    private String getType(String data){
        String result = "O";
        switch(data){
            case "Performance Assessment" : result = "P"; break;
            default : result = "O"; break;
        }
        return result;
    }


    /**
     * Getter
     */
    @Override
    public CourseListContainer getItem(int position) {
        return values.get(position);
    }

}

